import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/network/network_info.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_local_data_source.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_remote_data_source.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/repositories/menu_repository.dart';

class MenuRepositoryImplementation extends MenuRepository {
  final MenuRemoteDataSource remoteDataSource;
  final MenuLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  MenuRepositoryImplementation({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, FoodTagsList>> getFoodTagsList() async {
    if (await networkInfo.isConnected) {
      try {
        final FoodTagsList foodTagsList =
            await remoteDataSource.getFoodTagsList();
        localDataSource.cacheFoodTagsList(foodTagsList);
        return Right(foodTagsList);
      } on ServerException {
        return Left(ServerFailure());
      } on FireStoreException {
        return Left(FireStorageFailure());
      }
    } else {
      try {
        final FoodTagsList localFoodTagsList =
            await localDataSource.getFoodTagsList();
        return Right(localFoodTagsList);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, MenuComponentsList>> getMenuComponentsList() async {
    if (await networkInfo.isConnected) {
      try {
        final MenuComponentsList menuComponentsList =
            await remoteDataSource.getMenuComponentsList();
        localDataSource.cacheMenuComponentsList(menuComponentsList);
        return Right(menuComponentsList);
      } on ServerException {
        return Left(ServerFailure());
      } on FireStoreException {
        return Left(FireStorageFailure());
      }
    } else {
      try {
        final MenuComponentsList menuComponentsList =
            await localDataSource.getMenuComponentsList();
        return Right(menuComponentsList);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, MenuItemIconsList>> getMenuItemIconsList() async {
    if (await networkInfo.isConnected) {
      try {
        final MenuItemIconsList menuItemIconsList =
            await remoteDataSource.getMenuItemIconsList();
        localDataSource.cacheMenuItemIconsList(menuItemIconsList);
        return Right(menuItemIconsList);
      } on ServerException {
        return Left(ServerFailure());
      } on FireStoreException {
        return Left(FireStorageFailure());
      }
    } else {
      try {
        final MenuItemIconsList menuItemIconsList =
            await localDataSource.getMenuItemIconsList();
        return Right(menuItemIconsList);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, MenuItemsList>> getMenuItemsList(
      String restaurantId) async {
    if (await networkInfo.isConnected) {
      try {
        final MenuItemsList menuItemsList =
            await remoteDataSource.getMenuItemsList(restaurantId);
        localDataSource.cacheMenuItemsList(restaurantId, menuItemsList);
        return Right(menuItemsList);
      } on ServerException {
        return Left(ServerFailure());
      } on FireStoreException {
        return Left(FireStorageFailure());
      }
    } else {
      try {
        final MenuItemsList menuItemsList =
            await localDataSource.getMenuItemsList(restaurantId);
        return Right(menuItemsList);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, MenuList>> getMenusList(String restaurantId) async {
    if (await networkInfo.isConnected) {
      try {
        final MenuList menuList =
            await remoteDataSource.getMenusList(restaurantId);
        localDataSource.cacheMenusList(restaurantId, menuList);
        return Right(menuList);
      } on ServerException {
        return Left(ServerFailure());
      } on FireStoreException {
        return Left(FireStorageFailure());
      }
    } else {
      try {
        final MenuList menuList =
            await localDataSource.getMenusList(restaurantId);
        return Right(menuList);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
