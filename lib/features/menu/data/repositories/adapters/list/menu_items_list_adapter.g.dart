// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_items_list_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuItemsListAdapter extends TypeAdapter<MenuItemsList> {
  @override
  final int typeId = 7;

  @override
  MenuItemsList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuItemsList()
      ..menuItemsList = (fields[0] as List).cast<MenuItem>();
  }

  @override
  void write(BinaryWriter writer, MenuItemsList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.menuItemsList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuItemsListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
