// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_item_icons_list_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuItemIconsListAdapter extends TypeAdapter<MenuItemIconsList> {
  @override
  final int typeId = 10;

  @override
  MenuItemIconsList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuItemIconsList()
      ..menuItemIconsList = (fields[0] as List).cast<MenuItemIcon>();
  }

  @override
  void write(BinaryWriter writer, MenuItemIconsList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.menuItemIconsList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuItemIconsListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
