import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_item_adapter.dart';

part 'menu_items_list_adapter.g.dart';

@HiveType(typeId: 7)
class MenuItemsList extends HiveObject {
  @HiveField(0)
  late final List<MenuItem> menuItemsList;
}
