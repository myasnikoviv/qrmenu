// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_components_list_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuComponentsListAdapter extends TypeAdapter<MenuComponentsList> {
  @override
  final int typeId = 9;

  @override
  MenuComponentsList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuComponentsList()
      ..menuComponentsList = (fields[0] as List).cast<MenuComponent>();
  }

  @override
  void write(BinaryWriter writer, MenuComponentsList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.menuComponentsList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuComponentsListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
