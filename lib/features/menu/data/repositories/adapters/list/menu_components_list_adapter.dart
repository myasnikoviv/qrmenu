import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_component_adapter.dart';

part 'menu_components_list_adapter.g.dart';

@HiveType(typeId: 9)
class MenuComponentsList extends HiveObject {
  @HiveField(0)
  late final List<MenuComponent> menuComponentsList;
}
