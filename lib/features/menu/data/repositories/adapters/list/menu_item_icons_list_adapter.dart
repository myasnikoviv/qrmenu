import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_item_icon_adapter.dart';

part 'menu_item_icons_list_adapter.g.dart';

@HiveType(typeId: 10)
class MenuItemIconsList extends HiveObject {
  @HiveField(0)
  late final List<MenuItemIcon> menuItemIconsList;
}
