// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'food_tags_list_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FoodTagsListAdapter extends TypeAdapter<FoodTagsList> {
  @override
  final int typeId = 8;

  @override
  FoodTagsList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FoodTagsList()..foodTagsList = (fields[0] as List).cast<FoodTag>();
  }

  @override
  void write(BinaryWriter writer, FoodTagsList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.foodTagsList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FoodTagsListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
