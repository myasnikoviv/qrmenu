import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/food_tag_adapter.dart';

part 'food_tags_list_adapter.g.dart';

@HiveType(typeId: 8)
class FoodTagsList extends HiveObject {
  @HiveField(0)
  late final List<FoodTag> foodTagsList;
}
