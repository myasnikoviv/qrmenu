// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menus_list_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenusListAdapter extends TypeAdapter<MenusList> {
  @override
  final int typeId = 11;

  @override
  MenusList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenusList()..menusList = (fields[0] as List).cast<Menu>();
  }

  @override
  void write(BinaryWriter writer, MenusList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.menusList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenusListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
