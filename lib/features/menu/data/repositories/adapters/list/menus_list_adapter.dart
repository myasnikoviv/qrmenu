import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_adapter.dart';

part 'menus_list_adapter.g.dart';

@HiveType(typeId: 11)
class MenusList extends HiveObject {
  @HiveField(0)
  late final List<Menu> menusList;
}
