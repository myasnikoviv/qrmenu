import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'menu_item_adapter.g.dart';

@HiveType(typeId: 3)
class MenuItem extends HiveObject {
  @HiveField(0)
  late final String id;

  @HiveField(1)
  late final String name;

  @HiveField(2)
  late final String description;

  @HiveField(3)
  late final String menuId;

  @HiveField(4)
  late final double price;

  @HiveField(5)
  late final int? order;

  @HiveField(6)
  late final String? picture;

  @HiveField(7)
  late final List<dynamic> components;

  @HiveField(8)
  late final List<dynamic> foodTags;
}
