import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'food_tag_adapter.g.dart';

@HiveType(typeId: 0)
class FoodTag {
  @HiveField(0)
  late final String id;

  @HiveField(1)
  late final String name;

  @HiveField(2)
  late final String menuIconId;
}
