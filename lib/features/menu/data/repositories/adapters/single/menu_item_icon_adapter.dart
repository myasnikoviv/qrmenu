import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'menu_item_icon_adapter.g.dart';

@HiveType(typeId: 2)
class MenuItemIcon {
  @HiveField(0)
  late final String id;

  @HiveField(1)
  late final String icon;
}
