import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'menu_adapter.g.dart';

@HiveType(typeId: 4)
class Menu extends HiveObject {
  @HiveField(0)
  late final String id;

  @HiveField(1)
  late final String name;

  @HiveField(2)
  late final String description;

  @HiveField(3)
  late final String parentId;

  @HiveField(4)
  late final int order;

  @HiveField(5)
  late final int depth;
}
