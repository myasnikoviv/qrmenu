import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'menu_component_adapter.g.dart';

@HiveType(typeId: 1)
class MenuComponent {
  @HiveField(0)
  late final String id;

  @HiveField(1)
  late final String name;
}
