// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_item_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuItemAdapter extends TypeAdapter<MenuItem> {
  @override
  final int typeId = 3;

  @override
  MenuItem read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuItem()
      ..id = fields[0] as String
      ..name = fields[1] as String
      ..description = fields[2] as String
      ..menuId = fields[3] as String
      ..price = fields[4] as double
      ..order = fields[5] as int?
      ..picture = fields[6] as String?
      ..components = (fields[7] as List).cast<dynamic>()
      ..foodTags = (fields[8] as List).cast<dynamic>();
  }

  @override
  void write(BinaryWriter writer, MenuItem obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.description)
      ..writeByte(3)
      ..write(obj.menuId)
      ..writeByte(4)
      ..write(obj.price)
      ..writeByte(5)
      ..write(obj.order)
      ..writeByte(6)
      ..write(obj.picture)
      ..writeByte(7)
      ..write(obj.components)
      ..writeByte(8)
      ..write(obj.foodTags);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuItemAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
