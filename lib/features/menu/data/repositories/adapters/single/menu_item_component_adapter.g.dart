// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_item_component_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuItemComponentAdapter extends TypeAdapter<MenuItemComponent> {
  @override
  final int typeId = 6;

  @override
  MenuItemComponent read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuItemComponent()
      ..id = fields[0] as String
      ..name = fields[1] as String;
  }

  @override
  void write(BinaryWriter writer, MenuItemComponent obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuItemComponentAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
