// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_item_icon_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuItemIconAdapter extends TypeAdapter<MenuItemIcon> {
  @override
  final int typeId = 2;

  @override
  MenuItemIcon read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuItemIcon()
      ..id = fields[0] as String
      ..icon = fields[1] as String;
  }

  @override
  void write(BinaryWriter writer, MenuItemIcon obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.icon);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuItemIconAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
