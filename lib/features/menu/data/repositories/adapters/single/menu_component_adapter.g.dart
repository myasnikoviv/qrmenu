// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_component_adapter.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MenuComponentAdapter extends TypeAdapter<MenuComponent> {
  @override
  final int typeId = 1;

  @override
  MenuComponent read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MenuComponent()
      ..id = fields[0] as String
      ..name = fields[1] as String;
  }

  @override
  void write(BinaryWriter writer, MenuComponent obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MenuComponentAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
