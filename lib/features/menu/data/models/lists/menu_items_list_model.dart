import 'package:qrmenu/features/menu/data/models/single/menu_item_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';

class MenuItemsListModel extends MenuItemsList {
  final List<MenuItemModel> menuItems;

  MenuItemsListModel({required this.menuItems}) : super(menuItems: menuItems);

  factory MenuItemsListModel.fromJson(List<dynamic> json) {
    List<MenuItemModel> menuItemsList = [];
    json.forEach((element) {
      menuItemsList.add(MenuItemModel(
        id: element['id'],
        name: element['name'],
        description: element['description'],
        menuId: element['menuId'],
        price: element['price'],
        order: element['order'],
        picture: element['picture'],
        foodTags: element['foodTags'],
        components: element['components'],
      ));
    });
    return MenuItemsListModel(
      menuItems: menuItemsList,
    );
  }

  factory MenuItemsListModel.fromObject(List<Map<String, dynamic>> obj) {
    List<MenuItemModel> menuItemsList = [];
    obj.forEach((element) {
      menuItemsList.add(MenuItemModel(
        id: element['id'],
        name: element['name'],
        description: element['description'],
        menuId: element['menuId'],
        price: element['price'].toDouble(),
        order: element['order'],
        picture: element['picture'],
        foodTags: element['foodTags'],
        components: element['components'],
      ));
    });
    return MenuItemsListModel(
      menuItems: menuItemsList,
    );
  }
}
