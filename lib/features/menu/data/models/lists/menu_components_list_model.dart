import 'package:qrmenu/features/menu/data/models/single/menu_item_component_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';

class MenuComponentsListModel extends MenuComponentsList {
  final List<MenuItemComponentModel> components;

  MenuComponentsListModel({required this.components})
      : super(components: components);

  factory MenuComponentsListModel.fromJson(List<dynamic> json) {
    List<MenuItemComponentModel> menuComponentsList = [];
    json.forEach((element) {
      menuComponentsList.add(MenuItemComponentModel(
        id: element['id'],
        name: element['name'],
      ));
    });
    return MenuComponentsListModel(
      components: menuComponentsList,
    );
  }

  factory MenuComponentsListModel.fromObject(List<Map<String, dynamic>> obj) {
    List<MenuItemComponentModel> menuComponentsList = [];
    obj.forEach((element) {
      menuComponentsList.add(MenuItemComponentModel(
        id: element['id'],
        name: element['name'],
      ));
    });
    return MenuComponentsListModel(
      components: menuComponentsList,
    );
  }
}
