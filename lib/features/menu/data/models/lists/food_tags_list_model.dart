import 'package:qrmenu/features/menu/data/models/single/food_tag_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';

class FoodTagsListModel extends FoodTagsList {
  FoodTagsListModel({required List<FoodTagModel> foodTagsList})
      : super(foodTagsList: foodTagsList);

  factory FoodTagsListModel.fromJson(List<dynamic> json) {
    List<FoodTagModel> foodTagsList = [];
    json.forEach((element) {
      foodTagsList.add(FoodTagModel(
        id: element['id'],
        name: element['name'],
        menuIconId: element['menuIconId'],
      ));
    });
    return FoodTagsListModel(
      foodTagsList: foodTagsList,
    );
  }

  factory FoodTagsListModel.fromObject(List<Map<String, dynamic>> obj) {
    List<FoodTagModel> foodTagsList = [];
    obj.forEach((element) {
      foodTagsList.add(FoodTagModel(
        id: element['id'],
        name: element['name'],
        menuIconId: element['menuIconId'],
      ));
    });
    return FoodTagsListModel(
      foodTagsList: foodTagsList,
    );
  }
}
