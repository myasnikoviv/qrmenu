import 'package:qrmenu/features/menu/data/models/single/menu_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';

class MenuListModel extends MenuList {
  final List<Menu> menus;
  Iterator? menusIterator;

  MenuListModel({required this.menus}) : super(menus: menus) {
    menusIterator = menus.iterator;
  }

  factory MenuListModel.fromJson(List<dynamic> json) {
    List<MenuModel> menusList = [];
    json.forEach((element) {
      menusList.add(MenuModel(
        id: element['id'],
        name: element['name'],
        description: element['description'],
        parentId: element['parentId'],
        order: element['order'],
        depth: element['depth'],
      ));
    });
    return MenuListModel(
      menus: menusList,
    );
  }

  factory MenuListModel.fromObject(List<Map<String, dynamic>> obj) {
    List<MenuModel> menusList = [];
    obj.forEach((element) {
      menusList.add(MenuModel(
        id: element['id'],
        name: element['name'],
        description: element['description'],
        parentId: element['parentId'],
        order: element['order'],
        depth: element['depth'],
      ));
    });
    return MenuListModel(
      menus: menusList,
    );
  }
}
