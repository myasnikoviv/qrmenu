import 'package:qrmenu/features/menu/data/models/single/menu_item_icon_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';

class MenuItemIconsListModel extends MenuItemIconsList {
  final List<MenuItemIconModel> menuItemIcons;

  MenuItemIconsListModel({required this.menuItemIcons})
      : super(menuItemIcons: menuItemIcons);

  factory MenuItemIconsListModel.fromJson(List<dynamic> json) {
    List<MenuItemIconModel> menuItemIconsList = [];
    json.forEach((element) {
      menuItemIconsList.add(MenuItemIconModel(
        id: element['id'],
        icon: element['icon'],
      ));
    });
    return MenuItemIconsListModel(
      menuItemIcons: menuItemIconsList,
    );
  }

  factory MenuItemIconsListModel.fromObject(List<Map<String, dynamic>> obj) {
    List<MenuItemIconModel> menuItemIconsList = [];
    obj.forEach((element) {
      menuItemIconsList.add(MenuItemIconModel(
        id: element['id'],
        icon: element['icon'],
      ));
    });
    return MenuItemIconsListModel(
      menuItemIcons: menuItemIconsList,
    );
  }
}
