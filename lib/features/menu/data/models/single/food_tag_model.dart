import 'package:qrmenu/features/menu/domain/entities/single/food_tag.dart';

class FoodTagModel extends FoodTag {
  FoodTagModel({
    required String id,
    required String name,
    required String menuIconId,
  }) : super(
          id: id,
          name: name,
          menuIconId: menuIconId,
        );
}
