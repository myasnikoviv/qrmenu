import 'package:qrmenu/features/menu/domain/entities/single/menu_item_icon.dart';

class MenuItemIconModel extends MenuItemIcon {
  final String id;
  final String icon;

  MenuItemIconModel({
    required this.id,
    required this.icon,
  }) : super(id: id, icon: icon);
}
