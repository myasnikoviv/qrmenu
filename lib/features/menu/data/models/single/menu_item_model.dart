import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';

class MenuItemModel extends MenuItem {
  final String id;
  final String name;
  final String description;
  final String menuId;
  final double price;
  final List<dynamic> components;
  final List<dynamic> foodTags;
  final int? order;
  final String? picture;

  MenuItemModel({
    required this.id,
    required this.name,
    required this.description,
    required this.menuId,
    required this.price,
    required this.components,
    required this.foodTags,
    required this.order,
    this.picture,
  }) : super(
          id: id,
          name: name,
          description: description,
          menuId: menuId,
          price: price,
          components: components,
          foodTags: foodTags,
          order: order,
          picture: picture,
        );
}
