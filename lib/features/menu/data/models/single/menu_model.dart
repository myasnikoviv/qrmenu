import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';

class MenuModel extends Menu {
  final String id;
  final String name;
  final String description;
  final String parentId;
  final int order;
  final int depth;

  MenuModel({
    required this.id,
    required this.name,
    required this.description,
    required this.parentId,
    required this.order,
    required this.depth,
  }) : super(
          id: id,
          name: name,
          description: description,
          parentId: parentId,
          order: order,
          depth: depth,
        );
}
