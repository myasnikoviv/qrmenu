import 'package:qrmenu/features/menu/domain/entities/single/menu_item_component.dart';

class MenuItemComponentModel extends MenuItemComponent {
  final String id;
  final String name;

  MenuItemComponentModel({
    required this.id,
    required this.name,
  }) : super(
          id: id,
          name: name,
        );
}
