import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/models/lists/food_tags_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_components_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_item_icons_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_items_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_list_model.dart';

abstract class MenuRemoteDataSource {
  /// All methods call the Firebase Cloudstore
  ///
  /// Throws a [FireStoreException] for all error codes.

  Future<FoodTagsListModel> getFoodTagsList();

  Future<MenuComponentsListModel> getMenuComponentsList();

  Future<MenuItemIconsListModel> getMenuItemIconsList();

  Future<MenuItemsListModel> getMenuItemsList(String restaurantId);

  Future<MenuListModel> getMenusList(String restaurantId);
}

class MenuRemoteDataSourceImplementation implements MenuRemoteDataSource {
  final FirebaseFirestore firebaseFirestore;
  bool firestoreInitialized = false;

  MenuRemoteDataSourceImplementation({required this.firebaseFirestore});

  @override
  Future<FoodTagsListModel> getFoodTagsList() async {
    List<Map<String, dynamic>> foodTags = [];
    firebaseFirestore.collection('food_tags').snapshots().handleError((error) {
      throw FireStoreException();
    });
    if (await firebaseFirestore.collection('food_tags').snapshots().isEmpty) {
      throw FireStoreException();
    } else {
      await firebaseFirestore
          .collection('food_tags')
          .get()
          .then((QuerySnapshot querySnapshot) {
        if (querySnapshot.docs.isEmpty) {
          throw FireStoreException();
        } else {
          querySnapshot.docs.forEach((foodTag) {
            var keysExist = true;
            try {
              keysExist = foodTag.get('name') == null ? false : keysExist;
              keysExist = foodTag.get('menuIconId') == null ? false : keysExist;
            } catch (e) {
              keysExist = false;
            }
            if (!foodTag.exists || foodTag.id.isEmpty || !keysExist) {
              throw FireStoreException();
            } else {
              var firebaseFoodTag = {
                'id': foodTag.id,
                'name': foodTag.get('name'),
                'menuIconId': foodTag.get('menuIconId'),
              };
              foodTags.add(firebaseFoodTag);
            }
          });
        }
      });
    }

    if (foodTags.length == 0) {
      throw FireStoreException();
    } else {
      return FoodTagsListModel.fromObject(foodTags);
    }
  }

  @override
  Future<MenuComponentsListModel> getMenuComponentsList() async {
    List<Map<String, dynamic>> menuComponents = [];
    firebaseFirestore
        .collection('menu_components')
        .snapshots()
        .handleError((error) {
      throw FireStoreException();
    });
    if (await firebaseFirestore
        .collection('menu_components')
        .snapshots()
        .isEmpty) {
      throw FireStoreException();
    } else {
      await firebaseFirestore
          .collection('menu_components')
          .get()
          .then((QuerySnapshot querySnapshot) {
        if (querySnapshot.docs.isEmpty) {
          throw FireStoreException();
        } else {
          querySnapshot.docs.forEach((menuComponent) {
            var keysExist = true;
            try {
              keysExist = menuComponent.get('name') == null ? false : keysExist;
            } catch (e) {
              keysExist = false;
            }
            if (!menuComponent.exists ||
                menuComponent.id.isEmpty ||
                !keysExist) {
              throw FireStoreException();
            } else {
              var firebaseComponent = {
                'id': menuComponent.id,
                'name': menuComponent.get('name'),
              };
              menuComponents.add(firebaseComponent);
            }
          });
        }
      });
    }

    if (menuComponents.length == 0) {
      throw FireStoreException();
    } else {
      return MenuComponentsListModel.fromObject(menuComponents);
    }
  }

  @override
  Future<MenuItemIconsListModel> getMenuItemIconsList() async {
    List<Map<String, dynamic>> menuItemIconsList = [];
    firebaseFirestore.collection('menu_icons').snapshots().handleError((error) {
      throw FireStoreException();
    });
    if (await firebaseFirestore.collection('menu_icons').snapshots().isEmpty) {
      throw FireStoreException();
    } else {
      await firebaseFirestore
          .collection('menu_icons')
          .get()
          .then((QuerySnapshot querySnapshot) {
        if (querySnapshot.docs.isEmpty) {
          throw FireStoreException();
        } else {
          querySnapshot.docs.forEach((menuIcon) {
            var keysExist = true;
            try {
              keysExist = menuIcon.get('icon') == null ? false : keysExist;
            } catch (e) {
              keysExist = false;
            }
            if (!menuIcon.exists || menuIcon.id.isEmpty || !keysExist) {
              throw FireStoreException();
            } else {
              var firebaseItemIcon = {
                'id': menuIcon.id,
                'icon': menuIcon.get('icon'),
              };
              menuItemIconsList.add(firebaseItemIcon);
            }
          });
        }
      });
    }

    if (menuItemIconsList.length == 0) {
      throw FireStoreException();
    } else {
      return MenuItemIconsListModel.fromObject(menuItemIconsList);
    }
  }

  @override
  Future<MenuItemsListModel> getMenuItemsList(String restaurantId) async {
    List<Map<String, dynamic>> menuItemsList = [];
    await firebaseFirestore
        .collection('menu_items')
        .where('restaurantId', isEqualTo: restaurantId)
        .get()
        .onError((error, stackTrace) {
      throw FireStoreException();
    });
    await firebaseFirestore
        .collection('menu_items')
        .where('restaurantId', isEqualTo: restaurantId)
        .get()
        .then((value) async {
      if (value.docs.length == 0) {
        throw FireStoreException();
      } else {
        await firebaseFirestore
            .collection('menu_items')
            .where('restaurantId', isEqualTo: restaurantId)
            .get()
            .then((QuerySnapshot querySnapshot) {
          if (querySnapshot.docs.isEmpty) {
            throw FireStoreException();
          } else {
            querySnapshot.docs.forEach((menuItem) {
              var keysExist = true;
              try {
                keysExist = menuItem.get('name') == null ? false : keysExist;
                keysExist =
                    menuItem.get('description') == null ? false : keysExist;
                keysExist = menuItem.get('menuId') == null ? false : keysExist;
                keysExist = menuItem.get('price') == null ? false : keysExist;
                keysExist = menuItem.get('order') == null ? false : keysExist;
                keysExist = menuItem.get('picture') == null ? false : keysExist;
                keysExist =
                    menuItem.get('foodTags') == null ? false : keysExist;
                keysExist =
                    menuItem.get('components') == null ? false : keysExist;
              } catch (e) {
                keysExist = false;
              }
              if (!menuItem.exists || menuItem.id.isEmpty || !keysExist) {
                throw FireStoreException();
              } else {
                var firebaseItem = {
                  'id': menuItem.id,
                  'name': menuItem.get('name'),
                  'description': menuItem.get('description'),
                  'menuId': menuItem.get('menuId'),
                  'price': menuItem.get('price'),
                  'order': menuItem.get('order'),
                  'picture': menuItem.get('picture'),
                  'foodTags': menuItem.get('foodTags'),
                  'components': menuItem.get('components'),
                };
                menuItemsList.add(firebaseItem);
              }
            });
          }
        });
      }
    });
    if (menuItemsList.length == 0) {
      throw FireStoreException();
    } else {
      return MenuItemsListModel.fromObject(menuItemsList);
    }
  }

  @override
  Future<MenuListModel> getMenusList(String restaurantId) async {
    List<Map<String, dynamic>> menusList = [];
    await firebaseFirestore
        .collection('menus')
        .where('restaurantId', isEqualTo: restaurantId)
        .get()
        .onError((error, stackTrace) {
      throw FireStoreException();
    });

    await firebaseFirestore
        .collection('menus')
        .where('restaurantId', isEqualTo: restaurantId)
        .get()
        .then((value) async {
      if (value.docs.length == 0) {
        throw FireStoreException();
      } else {
        await firebaseFirestore
            .collection('menus')
            .where('restaurantId', isEqualTo: restaurantId)
            .get()
            .then((QuerySnapshot querySnapshot) {
          if (querySnapshot.docs.isEmpty) {
            throw FireStoreException();
          } else {
            querySnapshot.docs.forEach((menu) {
              var keysExist = true;
              try {
                keysExist = menu.get('name') == null ? false : keysExist;
                keysExist = menu.get('description') == null ? false : keysExist;
                keysExist = menu.get('parentId') == null ? false : keysExist;
                keysExist = menu.get('order') == null ? false : keysExist;
                keysExist = menu.get('depth') == null ? false : keysExist;
              } catch (e) {
                keysExist = false;
              }
              if (!menu.exists || menu.id.isEmpty || !keysExist) {
                throw FireStoreException();
              } else {
                var firebaseMenu = {
                  'id': menu.id,
                  'name': menu.get('name'),
                  'description': menu.get('description'),
                  'parentId': menu.get('parentId'),
                  'order': menu.get('order'),
                  'depth': menu.get('depth'),
                };
                menusList.add(firebaseMenu);
              }
            });
          }
        });
      }
    });

    if (menusList.length == 0) {
      throw FireStoreException();
    } else {
      return MenuListModel.fromObject(menusList);
    }
  }
}
