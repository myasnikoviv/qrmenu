import 'dart:io';

import 'package:hive/hive.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/models/lists/food_tags_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_components_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_item_icons_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_items_list_model.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_list_model.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/list/food_tags_list_adapter.dart'
    as FoodTagsListAdapter;
import 'package:qrmenu/features/menu/data/repositories/adapters/list/menu_components_list_adapter.dart'
    as MenuComponentsListAdapter;
import 'package:qrmenu/features/menu/data/repositories/adapters/list/menu_item_icons_list_adapter.dart'
    as MenuItemIconsListAdapter;
import 'package:qrmenu/features/menu/data/repositories/adapters/list/menu_items_list_adapter.dart'
    as MenuItemsListAdapter;
import 'package:qrmenu/features/menu/data/repositories/adapters/list/menus_list_adapter.dart'
    as MenusListAdapter;
import 'package:qrmenu/features/menu/data/repositories/adapters/single/food_tag_adapter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_adapter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_component_adapter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_item_adapter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_item_component_adapter.dart';
import 'package:qrmenu/features/menu/data/repositories/adapters/single/menu_item_icon_adapter.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';

abstract class MenuLocalDataSource {
  /// All methods get local JSON file
  ///
  /// Throws a [CacheException] for all error codes.

  Future<FoodTagsList> getFoodTagsList();

  Future<MenuComponentsList> getMenuComponentsList();

  Future<MenuItemIconsList> getMenuItemIconsList();

  Future<MenuItemsList> getMenuItemsList(String restaurantId);

  Future<MenuList> getMenusList(String restaurantId);

  Future<void> cacheFoodTagsList(FoodTagsList foodTagsList);

  Future<void> cacheMenuComponentsList(MenuComponentsList menuComponentsList);

  Future<void> cacheMenuItemIconsList(MenuItemIconsList menuItemIconsList);

  Future<void> cacheMenuItemsList(
      String restaurantId, MenuItemsList menuItemsList);

  Future<void> cacheMenusList(String restaurantId, MenuList menuListObj);
}

class MenuLocalDataSourceImplementation implements MenuLocalDataSource {
  final HiveInterface hiveInterface;
  final Directory appDocDir;

  MenuLocalDataSourceImplementation(
      {required this.appDocDir, required this.hiveInterface}) {
    hiveInterface.init(appDocDir.path);
    print(appDocDir.path);
    hiveInterface.registerAdapter(FoodTagAdapter());
    hiveInterface.registerAdapter(MenuComponentAdapter());
    hiveInterface.registerAdapter(MenuItemIconAdapter());
    hiveInterface.registerAdapter(MenuItemAdapter());
    hiveInterface.registerAdapter(MenuAdapter());
    hiveInterface.registerAdapter(MenuItemComponentAdapter());
    hiveInterface.registerAdapter(MenuItemsListAdapter.MenuItemsListAdapter());
    hiveInterface.registerAdapter(FoodTagsListAdapter.FoodTagsListAdapter());
    hiveInterface
        .registerAdapter(MenuComponentsListAdapter.MenuComponentsListAdapter());
    hiveInterface
        .registerAdapter(MenuItemIconsListAdapter.MenuItemIconsListAdapter());
    hiveInterface.registerAdapter(MenusListAdapter.MenusListAdapter());
  }

  @override
  Future<void> cacheFoodTagsList(FoodTagsList foodTagsList) async {
    final box = await getBox('foodTags_v1');
    FoodTagsListAdapter.FoodTagsList foodTagsListToCache =
        FoodTagsListAdapter.FoodTagsList();
    foodTagsListToCache.foodTagsList = [];
    foodTagsList.foodTagsList.forEach((element) {
      var foodTag = FoodTag()
        ..id = element.id
        ..menuIconId = element.menuIconId
        ..name = element.name;
      foodTagsListToCache.foodTagsList.add(foodTag);
    });
    return box.put('foodTags', foodTagsListToCache);
  }

  @override
  Future<void> cacheMenuComponentsList(
      MenuComponentsList menuComponentsList) async {
    final box = await getBox('menuComponents');
    MenuComponentsListAdapter.MenuComponentsList menuComponentsListToCache =
        MenuComponentsListAdapter.MenuComponentsList();
    menuComponentsListToCache.menuComponentsList = [];
    menuComponentsList.components.forEach((element) {
      var menuComponent = MenuComponent()
        ..id = element.id
        ..name = element.name;
      menuComponentsListToCache.menuComponentsList.add(menuComponent);
    });
    return box.put('menuComponents', menuComponentsListToCache);
  }

  @override
  Future<void> cacheMenuItemIconsList(
      MenuItemIconsList menuItemIconsList) async {
    final box = await getBox('menuIcons');
    MenuItemIconsListAdapter.MenuItemIconsList menuItemIconsListToCache =
        MenuItemIconsListAdapter.MenuItemIconsList();
    menuItemIconsListToCache.menuItemIconsList = [];
    menuItemIconsList.menuItemIcons.forEach((element) {
      var menuItemIcon = MenuItemIcon()
        ..id = element.id
        ..icon = element.icon;
      menuItemIconsListToCache.menuItemIconsList.add(menuItemIcon);
    });
    return box.put('menuIcons', menuItemIconsListToCache);
  }

  @override
  Future<void> cacheMenuItemsList(
      String restaurantId, MenuItemsList menuItemsList) async {
    final box = await getBox('menuItems_v2');
    MenuItemsListAdapter.MenuItemsList menuItemsListForCache =
        MenuItemsListAdapter.MenuItemsList();
    menuItemsListForCache.menuItemsList = [];
    box.delete(restaurantId);
    menuItemsList.menuItems.forEach((element) {
      var menuItem = MenuItem()
        ..id = element.id
        ..name = element.name
        ..description = element.description
        ..menuId = element.menuId
        ..price = element.price
        ..order = element.order != null ? element.order : 1
        ..picture = element.picture
        ..components = element.components
        ..foodTags = element.foodTags;
      menuItemsListForCache.menuItemsList.add(menuItem);
    });
    box.put(restaurantId, menuItemsListForCache);
    return await box.get(restaurantId);
  }

  @override
  Future<void> cacheMenusList(String restaurantId, MenuList menuList) async {
    final box = await getBox('menus_v1');
    box.delete(restaurantId);
    MenusListAdapter.MenusList menusListToCache = MenusListAdapter.MenusList();
    menusListToCache.menusList = [];
    menuList.menus.forEach((element) {
      var menu = Menu()
        ..id = element.id
        ..name = element.name
        ..description = element.description
        ..parentId = element.parentId
        ..order = element.order
        ..depth = element.depth;
      menusListToCache.menusList.add(menu);
    });
    box.put(restaurantId, menusListToCache);
    return await box.get(restaurantId);
  }

  @override
  Future<FoodTagsList> getFoodTagsList() async {
    if (await hiveInterface.boxExists('foodTags_v1')) {
      final box = await getBox('foodTags_v1');
      List<Map<String, dynamic>> foodTags = [];
      final cachedFoodTags = box.get('foodTags');
      cachedFoodTags.foodTagsList.forEach((foodTag) {
        Map<String, dynamic> currentItem = {
          "id": foodTag.id,
          "menuIconId": foodTag.menuIconId,
          "name": foodTag.name
        };
        foodTags.add(currentItem);
      });
      final FoodTagsList result = FoodTagsListModel.fromObject(foodTags);
      return result;
    } else {
      throw CacheException();
    }
  }

  @override
  Future<MenuComponentsList> getMenuComponentsList() async {
    if (await hiveInterface.boxExists('menuComponents')) {
      final box = await getBox('menuComponents');
      List<Map<String, dynamic>> menuComponents = [];
      final cachedMenuComponentsList = box.get('menuComponents');
      cachedMenuComponentsList.menuComponentsList.forEach((component) {
        Map<String, dynamic> currentItem = {
          "id": component.id,
          "name": component.name
        };
        menuComponents.add(currentItem);
      });
      final MenuComponentsList result =
          MenuComponentsListModel.fromObject(menuComponents);
      return result;
    } else {
      throw CacheException();
    }
  }

  @override
  Future<MenuItemIconsList> getMenuItemIconsList() async {
    if (await hiveInterface.boxExists('menuIcons')) {
      final box = await getBox('menuIcons');
      List<Map<String, dynamic>> menuItemIcons = [];
      final cachedMenuItemIconsList = box.get('MenuIcons');
      cachedMenuItemIconsList.menuItemIconsList.forEach((icon) {
        Map<String, dynamic> currentItem = {"id": icon.id, "icon": icon.icon};
        menuItemIcons.add(currentItem);
      });
      final MenuItemIconsList result =
          MenuItemIconsListModel.fromObject(menuItemIcons);
      return result;
    } else {
      throw CacheException();
    }
  }

  @override
  Future<MenuItemsList> getMenuItemsList(String restaurantId) async {
    if (await hiveInterface.boxExists('menuItems_v2')) {
      final boxItems = await getBox('menuItems_v2');
      var box = boxItems.get(restaurantId);
      List<Map<String, dynamic>> menuItemsList = [];
      box.menuItemsList.forEach((menuItem) {
        Map<String, dynamic> currentItem = {
          "id": menuItem.id,
          "name": menuItem.name,
          "description": menuItem.description,
          "menuId": menuItem.menuId,
          "price": menuItem.price,
          "order": menuItem.order != null ? menuItem.order : 1,
          "picture": menuItem.picture,
          "components": menuItem.components,
          "foodTags": menuItem.foodTags
        };
        menuItemsList.add(currentItem);
      });
      final MenuItemsList result = MenuItemsListModel.fromObject(menuItemsList);
      return result;
    } else {
      throw CacheException();
    }
  }

  @override
  Future<MenuList> getMenusList(String restaurantId) async {
    if (await hiveInterface.boxExists('menus_v1')) {
      final boxItems = await getBox('menus_v1');
      final cachedMenusList = boxItems.get(restaurantId);
      List<Map<String, dynamic>> menusList = [];
      cachedMenusList.menusList.forEach((menu) {
        Map<String, dynamic> currentItem = {
          "id": menu.id,
          "name": menu.name,
          "description": menu.description,
          "parentId": menu.parentId,
          "order": menu.order,
          "depth": menu.depth,
        };
        menusList.add(currentItem);
      });
      final MenuList result = MenuListModel.fromObject(menusList);
      return result;
    } else {
      throw CacheException();
    }
  }

  Future<Box> getBox(boxName) async {
    await hiveInterface.openBox(boxName);
    return hiveInterface.box(boxName);
  }
}
