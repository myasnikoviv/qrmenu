part of 'food_tags_list_bloc.dart';

abstract class FoodTagsListEvent extends Equatable {
  const FoodTagsListEvent();
}

class GetFoodTagsListEvent extends FoodTagsListEvent {
  GetFoodTagsListEvent();

  @override
  List<Object?> get props => [];
}
