part of 'food_tags_list_bloc.dart';

abstract class FoodTagsListState extends Equatable {
  const FoodTagsListState();
}

class Empty extends FoodTagsListState {
  @override
  List<Object> get props => [];
}

class Loading extends FoodTagsListState {
  @override
  List<Object> get props => [];
}

class FoodTagsListLoaded extends FoodTagsListState {
  final FoodTagsList foodTagsList;

  FoodTagsListLoaded({required this.foodTagsList});

  @override
  List<Object?> get props => [foodTagsList];
}

class Error extends FoodTagsListState {
  final String errorMsg;

  Error({required this.errorMsg});

  @override
  List<Object> get props => [errorMsg];
}
