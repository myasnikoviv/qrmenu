import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_food_tags_list.dart';

import '../error_handler.dart';

part 'food_tags_list_event.dart';

part 'food_tags_list_state.dart';

class FoodTagsListBloc extends Bloc<FoodTagsListEvent, FoodTagsListState> {
  final GetFoodTagsList getFoodTagsList;

  FoodTagsListBloc({required this.getFoodTagsList}) : super(Empty());

  @override
  Stream<FoodTagsListState> mapEventToState(
    FoodTagsListEvent event,
  ) async* {
    ErrorHandler errorHandler = ErrorHandler();
    if (event is GetFoodTagsListEvent) {
      yield Loading();
      final failureOrFoodTagsList = await getFoodTagsList(NoParams());
      yield failureOrFoodTagsList.fold(
          (failure) =>
              Error(errorMsg: errorHandler.mapFailureToMessage(failure)),
          (foodTagsList) => FoodTagsListLoaded(foodTagsList: foodTagsList));
    }
  }
}
