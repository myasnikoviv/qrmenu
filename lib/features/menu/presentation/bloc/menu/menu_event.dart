part of 'menu_bloc.dart';

abstract class MenuEvent extends Equatable {
  const MenuEvent();
}

class GetMenuByRestaurantIdEvent extends MenuEvent {
  final String restaurantId;

  GetMenuByRestaurantIdEvent(this.restaurantId);

  @override
  List<Object?> get props => [restaurantId];
}
