part of 'menu_bloc.dart';

abstract class MenuState extends Equatable {
  const MenuState();
}

class Empty extends MenuState {
  @override
  List<Object> get props => [];
}

class Loading extends MenuState {
  @override
  List<Object> get props => [];
}

class MenuLoaded extends MenuState {
  final MenuHierarchy menuHierarchy;

  MenuLoaded({required this.menuHierarchy});

  @override
  List<Object> get props => [menuHierarchy];
}

class Error extends MenuState {
  final String errorMsg;

  Error({required this.errorMsg});

  @override
  List<Object> get props => [errorMsg];
}
