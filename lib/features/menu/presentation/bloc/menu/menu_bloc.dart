import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:qrmenu/core/util/input_converter.dart';
import 'package:qrmenu/features/menu/domain/entities/hierarchy/menu_hierarchy.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_hierarchycal_menu.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menus_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';
import 'package:qrmenu/features/menu/presentation/bloc/error_handler.dart';

part 'menu_event.dart';

part 'menu_state.dart';

class MenuBloc extends Bloc<MenuEvent, MenuState> {
  final GetHierarchycalMenu getHierarchicalMenu;
  final GetMenuItemsList getMenuItemsList;
  final GetMenusList getMenusList;
  final InputConverter inputConverter;

  MenuBloc({
    required this.getHierarchicalMenu,
    required this.getMenuItemsList,
    required this.getMenusList,
    required this.inputConverter,
  }) : super(Empty());

  @override
  Stream<MenuState> mapEventToState(
    MenuEvent event,
  ) async* {
    ErrorHandler errorHandler = ErrorHandler();
    if (event is GetMenuByRestaurantIdEvent) {
      final inputEither = inputConverter.checkInputStringId(event.restaurantId);
      yield* inputEither.fold((failure) async* {
        yield Error(errorMsg: INPUT_FAILURE_MESSAGE);
      }, (restaurantId) async* {
        yield Loading();
        final failureOrMenuItemsList =
            await getMenuItemsList(Params(restaurantId: restaurantId));
        yield* failureOrMenuItemsList.fold((failure) async* {
          yield Error(errorMsg: errorHandler.mapFailureToMessage(failure));
        }, (menuItemsList) async* {
          final failureOrMenusList =
              await getMenusList(Params(restaurantId: restaurantId));
          yield* failureOrMenusList.fold((failure) async* {
            yield Error(errorMsg: errorHandler.mapFailureToMessage(failure));
          }, (menusList) async* {
            final failureOrHierarchicalMenu =
                await getHierarchicalMenu(ParamsHierarchy(
              menusList: menusList,
              menuItemsList: menuItemsList,
            ));
            yield failureOrHierarchicalMenu.fold(
                (failure) =>
                    Error(errorMsg: errorHandler.mapFailureToMessage(failure)),
                (menuHierarchy) => MenuLoaded(menuHierarchy: menuHierarchy));
          });
        });
      });
    }
  }
}
