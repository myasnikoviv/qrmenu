import 'package:qrmenu/features/menu/presentation/bloc/menu/menu_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_components/menu_components_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_item_icons/menu_item_icons_list_bloc.dart';
import 'package:qrmenu/injection_container.dart';

import 'food_tags/food_tags_list_bloc.dart';

class Blocs {
  static final Blocs _instance = Blocs._internal();
  MenuBloc _menuBloc = sl<MenuBloc>();
  FoodTagsListBloc _foodTagsListBloc = sl<FoodTagsListBloc>();
  MenuComponentsListBloc _menuComponentsListBloc = sl<MenuComponentsListBloc>();
  MenuItemIconsListBloc _menuItemIconsListBloc = sl<MenuItemIconsListBloc>();

  factory Blocs() => _instance;

  Blocs._internal();

  MenuBloc get menu => _menuBloc;

  FoodTagsListBloc get foodTags => _foodTagsListBloc;

  MenuComponentsListBloc get components => _menuComponentsListBloc;

  MenuItemIconsListBloc get icons => _menuItemIconsListBloc;
}
