part of 'menu_components_list_bloc.dart';

@immutable
abstract class MenuComponentsListState extends Equatable {
  const MenuComponentsListState();
}

class Empty extends MenuComponentsListState {
  @override
  List<Object> get props => [];
}

class Loading extends MenuComponentsListState {
  @override
  List<Object> get props => [];
}

class MenuComponentsListLoaded extends MenuComponentsListState {
  final MenuComponentsList menuComponentsList;

  MenuComponentsListLoaded({required this.menuComponentsList});

  @override
  List<Object?> get props => [menuComponentsList];
}

class Error extends MenuComponentsListState {
  final String errorMsg;

  Error({required this.errorMsg});

  @override
  List<Object> get props => [errorMsg];
}
