part of 'menu_components_list_bloc.dart';

abstract class MenuComponentsListEvent extends Equatable {
  const MenuComponentsListEvent();
}

class GetMenuComponentsListEvent extends MenuComponentsListEvent {
  GetMenuComponentsListEvent();

  @override
  List<Object?> get props => [];
}
