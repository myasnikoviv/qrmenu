import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_components_list.dart';

import '../error_handler.dart';

part 'menu_components_list_event.dart';

part 'menu_components_list_state.dart';

class MenuComponentsListBloc
    extends Bloc<MenuComponentsListEvent, MenuComponentsListState> {
  final GetMenuComponentsList getMenuComponentsList;

  MenuComponentsListBloc({required this.getMenuComponentsList})
      : super(Empty());

  @override
  Stream<MenuComponentsListState> mapEventToState(
    MenuComponentsListEvent event,
  ) async* {
    ErrorHandler errorHandler = ErrorHandler();
    if (event is GetMenuComponentsListEvent) {
      yield Loading();
      final failureOrMenuComponentsList =
          await getMenuComponentsList(NoParams());
      yield failureOrMenuComponentsList.fold(
          (failure) =>
              Error(errorMsg: errorHandler.mapFailureToMessage(failure)),
          (menuComponentsList) =>
              MenuComponentsListLoaded(menuComponentsList: menuComponentsList));
    }
  }
}
