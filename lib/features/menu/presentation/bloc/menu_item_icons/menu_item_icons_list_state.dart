part of 'menu_item_icons_list_bloc.dart';

abstract class MenuItemIconsListState extends Equatable {
  const MenuItemIconsListState();
}

class Empty extends MenuItemIconsListState {
  @override
  List<Object> get props => [];
}

class Loading extends MenuItemIconsListState {
  @override
  List<Object> get props => [];
}

class MenuItemIconsListLoaded extends MenuItemIconsListState {
  final MenuItemIconsList menuItemIconsList;

  MenuItemIconsListLoaded({required this.menuItemIconsList});

  @override
  List<Object?> get props => [menuItemIconsList];
}

class Error extends MenuItemIconsListState {
  final String errorMsg;

  Error({required this.errorMsg});

  @override
  List<Object> get props => [errorMsg];
}
