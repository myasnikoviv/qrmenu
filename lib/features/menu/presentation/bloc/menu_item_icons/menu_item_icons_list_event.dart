part of 'menu_item_icons_list_bloc.dart';

abstract class MenuItemIconsListEvent extends Equatable {
  const MenuItemIconsListEvent();
}

class GetMenuItemIconsListEvent extends MenuItemIconsListEvent {
  GetMenuItemIconsListEvent();

  @override
  List<Object?> get props => [];
}
