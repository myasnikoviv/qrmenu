import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_item_icons_list.dart';

import '../error_handler.dart';

part 'menu_item_icons_list_event.dart';

part 'menu_item_icons_list_state.dart';

class MenuItemIconsListBloc
    extends Bloc<MenuItemIconsListEvent, MenuItemIconsListState> {
  GetMenuItemIconsList getMenuItemIconsList;

  MenuItemIconsListBloc({required this.getMenuItemIconsList}) : super(Empty());

  @override
  Stream<MenuItemIconsListState> mapEventToState(
    MenuItemIconsListEvent event,
  ) async* {
    ErrorHandler errorHandler = ErrorHandler();
    if (event is GetMenuItemIconsListEvent) {
      yield Loading();
      final failureOrMenuItemIconsList = await getMenuItemIconsList(NoParams());
      yield failureOrMenuItemIconsList.fold(
          (failure) =>
              Error(errorMsg: errorHandler.mapFailureToMessage(failure)),
          (menuItemIconsList) =>
              MenuItemIconsListLoaded(menuItemIconsList: menuItemIconsList));
    }
  }
}
