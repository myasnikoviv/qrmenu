import 'package:qrmenu/core/error/failure.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INPUT_FAILURE_MESSAGE = 'Invalid Input. Should be valid Id';
const String OTHER_FAILURE_MESSAGE = 'Some error occurs';

class ErrorHandler {
  String mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return OTHER_FAILURE_MESSAGE;
    }
  }
}
