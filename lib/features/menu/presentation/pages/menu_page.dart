import 'package:flutter/material.dart';
import 'package:qrmenu/features/menu/presentation/widgets/menu_page_build_bloc_provider.dart';

import '../../../../core/presentation/pages/main_template.dart';

class MenuPage extends StatelessWidget {
  MenuPage() : super();

  @override
  Widget build(BuildContext context) {
    return MainTemplate(
      child: MenuPageBuildBlocProvider(),
    );
  }
}
