import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_components/menu_components_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/widgets/state_icon.dart';

class MenuComponentsListLoad extends StatelessWidget {
  const MenuComponentsListLoad({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MenuComponentsListBloc, MenuComponentsListState>(
        builder: (context, state) {
      if (state is Empty) {
        return const StateIcon(type: 'Empty', icon: Icons.more);
      } else if (state is Error) {
        return const StateIcon(type: 'Error', icon: Icons.more);
      } else if (state is MenuComponentsListLoaded) {
        return const StateIcon(type: 'Loaded', icon: Icons.more);
      } else if (state is Loading) {
        return const StateIcon(type: 'Loading', icon: Icons.more);
      } else {
        return const StateIcon(type: 'Empty', icon: Icons.more);
      }
    });
  }
}
