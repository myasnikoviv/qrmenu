import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';

class MenuItemContentNotExpanded extends StatelessWidget {
  const MenuItemContentNotExpanded({
    Key? key,
    required this.menuItem,
    required this.f,
  }) : super(key: key);

  final MenuItem menuItem;
  final NumberFormat f;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          height: 130,
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  left: 20,
                ),
                alignment: AlignmentDirectional.centerStart,
                height: 60,
                child: Text(
                  this.menuItem.name,
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: const EdgeInsets.only(left: 20),
                alignment: AlignmentDirectional.topStart,
                height: 40,
                child: Text(
                  f.format(this.menuItem.price),
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
