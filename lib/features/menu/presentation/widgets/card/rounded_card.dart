import 'package:flutter/material.dart';

class RoundedCard extends StatelessWidget {
  const RoundedCard({
    Key? key,
    required this.childWidget,
    this.ifColor = false,
    this.isExpanded = false,
  }) : super(key: key);

  final Widget childWidget;
  final bool ifColor;
  final bool isExpanded;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: (ifColor && isExpanded)
            ? BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))
            : BorderRadius.all(Radius.circular(20)),
        color: this.ifColor
            ? Colors.black.withOpacity(0.4)
            : Colors.black.withOpacity(0),
      ),
      child: this.childWidget,
    );
  }
}
