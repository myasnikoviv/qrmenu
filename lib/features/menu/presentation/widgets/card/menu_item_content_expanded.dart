import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';

import 'menu_components_chips.dart';

class MenuItemContentExpanded extends StatelessWidget {
  const MenuItemContentExpanded({
    Key? key,
    required this.menuItem,
    required this.f,
  }) : super(key: key);

  final MenuItem menuItem;
  final NumberFormat f;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(children: [
        SizedBox(
          height: 100,
        ),
        Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Container(
                alignment: AlignmentDirectional.centerStart,
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                  this.menuItem.name,
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
              Container(
                alignment: AlignmentDirectional.centerStart,
                child: Text(this.menuItem.description),
              ),
              SizedBox(
                height: 15,
              ),
              Container(child: menuComponentsChips(menuItem)),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  alignment: AlignmentDirectional.topStart,
                  child: Text(
                    f.format(this.menuItem.price),
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
