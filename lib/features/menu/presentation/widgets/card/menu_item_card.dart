import 'package:flutter/material.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';
import 'package:qrmenu/features/menu/presentation/widgets/card/food_tags_chips.dart';
import 'package:qrmenu/features/menu/presentation/widgets/card/menu_item_content.dart';
import 'package:qrmenu/features/menu/presentation/widgets/card/rounded_card.dart';

import 'card_picture.dart';

class MenuItemCard extends StatefulWidget {
  const MenuItemCard({
    Key? key,
    required this.menuItem,
  }) : super(key: key);

  final MenuItem menuItem;

  @override
  _MenuItemCardState createState() => _MenuItemCardState();
}

class _MenuItemCardState extends State<MenuItemCard> {
  bool _isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _isExpanded = !_isExpanded;
        setState(() {});
      },
      child: Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 3,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Stack(
            children: [
              RoundedCard(
                childWidget: menuItemImageHelper(widget.menuItem, _isExpanded),
                isExpanded: _isExpanded,
              ),
              RoundedCard(
                childWidget: MenuItemContent(
                  menuItem: widget.menuItem,
                  isExpanded: _isExpanded,
                ),
                ifColor: false,
                isExpanded: _isExpanded,
              ),
              FoodTagsChips(menuItem: widget.menuItem),
            ],
          ),
        ),
      ),
    );
  }
}

Widget menuItemImageHelper(menuItem, isExpanded) {
  if (menuItem.picture != null && menuItem.picture!.length > 0) {
    return CardPicture(
      url: menuItem.picture,
      isExpanded: isExpanded,
    );
  } else {
    return Container();
  }
}
