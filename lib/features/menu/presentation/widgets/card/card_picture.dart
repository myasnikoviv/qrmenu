import 'package:flutter/material.dart';
import 'package:qrmenu/features/menu/presentation/widgets/card/rounded_card.dart';

class CardPicture extends StatelessWidget {
  const CardPicture({
    Key? key,
    required this.url,
    this.isExpanded = false,
  }) : super(key: key);

  final String url;
  final bool isExpanded;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        height: isExpanded ? 100 : 200,
        child: Stack(children: [
          ClipRRect(
            borderRadius: isExpanded
                ? BorderRadius.only(
                    topLeft: Radius.circular(20), topRight: Radius.circular(20))
                : BorderRadius.all(
                    Radius.circular(20),
                  ),
            child: Center(
              child: FadeInImage(
                width: MediaQuery.of(context).size.width,
                height: isExpanded ? 100 : 200,
                fit: BoxFit.cover,
                image: NetworkImage(this.url),
                placeholder: AssetImage('assets/image-placeholder.png'),
              ),
            ),
          ),
          RoundedCard(
            childWidget: Container(),
            ifColor: true,
            isExpanded: isExpanded,
          ),
        ]),
      ),
    );
  }
}
