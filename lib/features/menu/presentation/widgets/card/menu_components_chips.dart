import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/blocs.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_components/menu_components_list_bloc.dart';

import 'chips_list.dart';

Widget menuComponentsChips(menuItem) {
  return BlocProvider(
    create: (BuildContext context) {
      Blocs blocs = Blocs();
      return blocs.components;
    },
    child: BlocBuilder<MenuComponentsListBloc, MenuComponentsListState>(
      builder: (BuildContext context, state) {
        if (state is MenuComponentsListLoaded) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              chipList(
                menuItemIdsList: menuItem.components,
                allElementsList: state.menuComponentsList.components,
                type: 'component',
                buildChip: _buildChip,
              ),
              SizedBox(
                height: 15,
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    ),
  );
}

Widget _buildChip(
    {required String label,
    required Color color,
    String menuIconId = 'undefined'}) {
  return Chip(
    labelPadding: EdgeInsets.all(2.0),
    avatar: Text(label[0].toUpperCase()),
    label: Text(
      label,
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    backgroundColor: color,
    elevation: 6.0,
    shadowColor: Colors.grey[60],
    padding: EdgeInsets.all(8.0),
  );
}
