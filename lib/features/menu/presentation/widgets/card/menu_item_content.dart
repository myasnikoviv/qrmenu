import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';
import 'package:qrmenu/features/menu/presentation/widgets/card/menu_item_content_not_expanded.dart';

import 'menu_item_content_expanded.dart';

class MenuItemContent extends StatelessWidget {
  const MenuItemContent({
    Key? key,
    required this.menuItem,
    this.isExpanded = false,
  }) : super(key: key);

  final MenuItem menuItem;
  final isExpanded;

  @override
  Widget build(BuildContext context) {
    final f = NumberFormat.currency(locale: "uk_UA");
    return isExpanded
        ? MenuItemContentExpanded(menuItem: menuItem, f: f)
        : MenuItemContentNotExpanded(menuItem: menuItem, f: f);
  }
}
