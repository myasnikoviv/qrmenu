import 'package:flutter/material.dart';

Widget chipList({
  required List<dynamic> menuItemIdsList,
  required List<dynamic> allElementsList,
  required String type,
  required Function buildChip,
}) {
  List<Widget> chipsList = [];
  menuItemIdsList.forEach((itemCurrentId) {
    allElementsList.forEach((element) {
      if (itemCurrentId == element.id) {
        if (type == 'foodTag') {
          chipsList.add(buildChip(
            label: element.name,
            color: Colors.lightGreen,
            menuIconId: element.menuIconId,
          ));
        } else {
          chipsList.add(buildChip(
            label: element.name,
            color: Colors.blue,
          ));
        }
      }
    });
  });
  return Wrap(
    spacing: 6.0,
    runSpacing: 6.0,
    children: chipsList,
  );
}
