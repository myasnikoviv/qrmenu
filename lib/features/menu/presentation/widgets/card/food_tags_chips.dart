import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';
import 'package:qrmenu/features/menu/presentation/bloc/blocs.dart';
import 'package:qrmenu/features/menu/presentation/bloc/food_tags/food_tags_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_item_icons/menu_item_icons_list_bloc.dart';

import 'chips_list.dart';

class FoodTagsChips extends StatelessWidget {
  const FoodTagsChips({Key? key, required this.menuItem}) : super(key: key);

  final MenuItem menuItem;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        Blocs blocs = Blocs();
        return blocs.foodTags;
      },
      child: BlocBuilder<FoodTagsListBloc, FoodTagsListState>(
        builder: (BuildContext context, state) {
          if (state is FoodTagsListLoaded) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: chipList(
                menuItemIdsList: menuItem.foodTags,
                allElementsList: state.foodTagsList.foodTagsList,
                type: 'foodTag',
                buildChip: _buildChip,
              ),
            );
          } else
            return Container();
        },
      ),
    );
  }

  Widget _buildChip(
      {required String label,
      required Color color,
      String menuIconId = 'undefined'}) {
    return Chip(
      labelPadding: EdgeInsets.all(2.0),
      avatar: BlocProvider(
        create: (BuildContext context) {
          Blocs blocs = Blocs();
          return blocs.icons;
        },
        child: BlocBuilder<MenuItemIconsListBloc, MenuItemIconsListState>(
          builder: (BuildContext context, state) {
            Widget insideWidget = Text(label[0].toUpperCase());

            if (state is MenuItemIconsListLoaded) {
              state.menuItemIconsList.menuItemIcons.forEach((icon) {
                if (icon.id == menuIconId) {
                  if (int.parse(icon.icon) > 0) {
                    insideWidget = Icon(
                      IconData(int.parse(icon.icon),
                          fontFamily: 'MaterialIcons'),
                      color: Colors.green.shade50,
                      size: 15,
                    );
                  }
                }
              });
            }
            return CircleAvatar(
              backgroundColor: Colors.white70,
              child: insideWidget,
            );
          },
        ),
      ),
      label: Text(
        label,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: color,
      elevation: 6.0,
      shadowColor: Colors.grey[60],
      padding: EdgeInsets.all(8.0),
    );
  }
}
