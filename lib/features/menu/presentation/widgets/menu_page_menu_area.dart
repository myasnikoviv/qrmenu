import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu/menu_bloc.dart';

import 'menu_page_loading_widget.dart';
import 'menu_page_menu_display.dart';
import 'menu_page_message_display.dart';

class MenuArea extends StatelessWidget {
  const MenuArea({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MenuBloc, MenuState>(
      builder: (context, state) {
        if (state is Empty) {
          return const MessageDisplay(
            msg: 'Меню не загружены',
          );
        } else if (state is Error) {
          return MessageDisplay(
            msg: state.errorMsg,
          );
        } else if (state is MenuLoaded) {
          return MenuDisplay(
            menuHierarchy: state.menuHierarchy,
          );
        } else if (state is Loading) {
          return const LoadingWidget();
        } else {
          return Container();
        }
      },
    );
  }
}
