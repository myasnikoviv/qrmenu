import 'package:flutter/material.dart';
import 'package:qrmenu/features/menu/presentation/bloc/blocs.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu/menu_bloc.dart';

class MenuControls extends StatefulWidget {
  const MenuControls({
    Key? key,
  }) : super(key: key);

  @override
  _MenuControlsState createState() => _MenuControlsState();
}

class _MenuControlsState extends State<MenuControls> {
  String inputStr = '';
  final controller = TextEditingController();

  void dispatchGetMenu() {
    controller.clear();
    Blocs blocs = Blocs();
    blocs.menu.add(GetMenuByRestaurantIdEvent(inputStr));
    FocusScope.of(context).unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: controller,
          onChanged: (value) {
            inputStr = value;
          },
          onSubmitted: (_) {
            dispatchGetMenu();
          },
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Input a number',
          ),
          keyboardType: TextInputType.number,
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                onPressed: dispatchGetMenu,
                child: Text('Search'),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
