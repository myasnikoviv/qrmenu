import 'package:flutter/material.dart';
import 'package:qrmenu/features/menu/domain/entities/hierarchy/menu_hierarchy.dart';

import 'menu_level.dart';

class MenuDisplay extends StatelessWidget {
  final MenuHierarchy menuHierarchy;

  const MenuDisplay({
    Key? key,
    required this.menuHierarchy,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        padding: const EdgeInsets.all(8),
        scrollDirection: Axis.vertical,
        shrinkWrap: false,
        children: menuLevel(menuHierarchy, context),
      ),
    );
  }
}
