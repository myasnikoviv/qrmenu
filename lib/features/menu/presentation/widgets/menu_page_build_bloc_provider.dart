import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/blocs.dart';

import 'menu_page_menu_area.dart';

class MenuPageBuildBlocProvider extends StatelessWidget {
  const MenuPageBuildBlocProvider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        Blocs blocs = Blocs();
        return blocs.menu;
      },
      child: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height - 90,
                  child: MenuArea(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
