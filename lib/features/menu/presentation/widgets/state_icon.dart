import 'package:flutter/material.dart';

class StateIcon extends StatefulWidget {
  const StateIcon({
    Key? key,
    required this.type,
    required this.icon,
  }) : super(key: key);

  final String type;
  final IconData icon;

  @override
  _StateIconState createState() => _StateIconState();
}

class _StateIconState extends State<StateIcon> {
  @override
  Widget build(BuildContext context) {
    MaterialColor color = Colors.grey;

    if (widget.type == 'Empty') {
      color = Colors.grey;
    } else if (widget.type == 'Loading') {
      color = Colors.blue;
    } else if (widget.type == 'Error') {
      color = Colors.red;
    } else if (widget.type == 'Loaded') {
      color = Colors.green;
    }
    return Icon(widget.icon, color: color);
  }

  Animation getAnimation(AnimationController controller) {
    return Tween(begin: 0.2, end: 0.5).animate(controller)
      ..addListener(() {
        setState(() {});
      });
  }
}
