import 'package:flutter/material.dart';
import 'package:qrmenu/features/menu/domain/entities/hierarchy/menu_hierarchy.dart';
import 'package:qrmenu/features/menu/presentation/widgets/card/menu_item_card.dart';

List<Widget> menuLevel(MenuHierarchy menuHierarchy, BuildContext context) {
  return [
    Padding(
      padding: const EdgeInsets.only(top: 0, bottom: 20),
      child: menuHierarchy.menu!.id == 'rootMenu'
          ? const SizedBox()
          : Column(
              children: [
                const SizedBox(
                  height: 15,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 3,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: ListTile(
                    title: Text(menuHierarchy.menu!.name),
                    subtitle: Text(menuHierarchy.menu!.description),
                  ),
                ),
              ],
            ),
    ),
    ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: menuHierarchy.menuItems!.length,
      itemBuilder: (context, index) {
        return MenuItemCard(
          menuItem: menuHierarchy.menuItems![index],
        );
      },
    ),
    ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: menuHierarchy.nested!.length,
      itemBuilder: (context, index) {
        return ListView(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: menuLevel(menuHierarchy.nested![index], context),
        );
      },
    ),
  ];
}
