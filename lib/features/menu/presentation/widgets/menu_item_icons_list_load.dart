import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_item_icons/menu_item_icons_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/widgets/state_icon.dart';

class MenuItemIconsListLoad extends StatelessWidget {
  const MenuItemIconsListLoad({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MenuItemIconsListBloc, MenuItemIconsListState>(
        builder: (context, state) {
      if (state is Empty) {
        return const StateIcon(type: 'Empty', icon: Icons.image);
      } else if (state is Error) {
        return const StateIcon(type: 'Error', icon: Icons.image);
      } else if (state is MenuItemIconsListLoaded) {
        return const StateIcon(type: 'Loaded', icon: Icons.image);
      } else if (state is Loading) {
        return const StateIcon(type: 'Loading', icon: Icons.image);
      } else {
        return const StateIcon(type: 'Empty', icon: Icons.image);
      }
    });
  }
}
