import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/food_tags/food_tags_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/widgets/state_icon.dart';

class FoodTagsListLoad extends StatelessWidget {
  const FoodTagsListLoad({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FoodTagsListBloc, FoodTagsListState>(
        builder: (context, state) {
      if (state is Empty) {
        return const StateIcon(type: 'Empty', icon: Icons.local_offer);
      } else if (state is Error) {
        return const StateIcon(type: 'Error', icon: Icons.local_offer);
      } else if (state is FoodTagsListLoaded) {
        return const StateIcon(type: 'Loaded', icon: Icons.local_offer);
      } else if (state is Loading) {
        return const StateIcon(type: 'Loading', icon: Icons.local_offer);
      } else {
        return const StateIcon(type: 'Empty', icon: Icons.local_offer);
      }
    });
  }
}
