import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';

abstract class MenuRepository {
  Future<Either<Failure, MenuList>> getMenusList(String restaurantId);

  Future<Either<Failure, MenuItemsList>> getMenuItemsList(String restaurantId);

  Future<Either<Failure, MenuComponentsList>> getMenuComponentsList();

  Future<Either<Failure, MenuItemIconsList>> getMenuItemIconsList();

  Future<Either<Failure, FoodTagsList>> getFoodTagsList();
}
