import 'package:equatable/equatable.dart';

class MenuItemComponent extends Equatable {
  final String id;
  final String name;

  MenuItemComponent({
    required this.id,
    required this.name,
  });

  @override
  List<Object?> get props => [id, name];
}
