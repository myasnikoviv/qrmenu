import 'package:equatable/equatable.dart';

class MenuItemIcon extends Equatable {
  final String id;
  final String icon;

  MenuItemIcon({
    required this.id,
    required this.icon,
  });

  @override
  List<Object?> get props => [id, icon];
}
