import 'package:equatable/equatable.dart';

class FoodTag extends Equatable {
  final String id;
  final String name;
  final String menuIconId;

  FoodTag({
    required this.id,
    required this.name,
    required this.menuIconId,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        menuIconId,
      ];
}
