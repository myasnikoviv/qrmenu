import 'package:equatable/equatable.dart';

class Menu extends Equatable {
  final String id;
  final String name;
  final String description;
  final String parentId;
  final int order;
  final int depth;

  Menu({
    required this.id,
    required this.name,
    required this.description,
    required this.parentId,
    required this.order,
    required this.depth,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        parentId,
        order,
        depth,
      ];
}
