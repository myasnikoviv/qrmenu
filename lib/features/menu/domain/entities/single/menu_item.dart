import 'package:equatable/equatable.dart';

class MenuItem extends Equatable {
  final String id;
  final String name;
  final String description;
  final String menuId;
  final double price;
  final List<dynamic> components;
  final List<dynamic> foodTags;
  final int? order;
  final String? picture;

  MenuItem({
    required this.id,
    required this.name,
    required this.description,
    required this.menuId,
    required this.price,
    required this.components,
    required this.foodTags,
    required this.order,
    this.picture,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        menuId,
        price,
        components,
        foodTags,
        order,
        picture,
      ];
}
