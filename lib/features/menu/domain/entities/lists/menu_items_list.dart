import 'package:equatable/equatable.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';

class MenuItemsList extends Equatable {
  final List<MenuItem> menuItems;

  MenuItemsList({required this.menuItems});

  @override
  List<Object?> get props => [menuItems];
}
