import 'package:equatable/equatable.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_icon.dart';

class MenuItemIconsList extends Equatable {
  final List<MenuItemIcon> menuItemIcons;

  MenuItemIconsList({required this.menuItemIcons});

  @override
  List<Object?> get props => [menuItemIcons];
}
