import 'package:equatable/equatable.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';

class MenuList extends Equatable {
  final List<Menu> menus;

  MenuList({required this.menus});

  @override
  List<Object?> get props => [menus];
}
