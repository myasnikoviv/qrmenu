import 'package:equatable/equatable.dart';
import 'package:qrmenu/features/menu/domain/entities/single/food_tag.dart';

class FoodTagsList extends Equatable {
  final List<FoodTag> foodTagsList;

  FoodTagsList({required this.foodTagsList});

  @override
  List<Object?> get props => [foodTagsList];
}
