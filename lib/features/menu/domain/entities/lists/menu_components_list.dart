import 'package:equatable/equatable.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_component.dart';

class MenuComponentsList extends Equatable {
  final List<MenuItemComponent> components;

  MenuComponentsList({required this.components});

  @override
  List<Object?> get props => [components];
}
