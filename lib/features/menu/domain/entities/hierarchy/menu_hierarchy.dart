import 'package:equatable/equatable.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';

class MenuHierarchy extends Equatable {
  List<MenuHierarchy>? nested;
  List<MenuItem>? menuItems;
  Menu? menu;

  MenuHierarchy({
    this.nested,
    this.menuItems,
    this.menu,
  }) {
    nested = nested != null ? nested : [];
    menuItems = menuItems != null ? menuItems : [];
  }

  @override
  List<Object?> get props => [
        nested,
        menuItems,
        menu,
      ];
}
