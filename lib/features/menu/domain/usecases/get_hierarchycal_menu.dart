import 'dart:collection';

import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/hierarchy/menu_hierarchy.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';

class GetHierarchycalMenu implements UseCase<MenuHierarchy, ParamsHierarchy> {
  MenuHierarchy rootMenuHierarchy = MenuHierarchy();
  Map<int, Map<int, List<Menu>>> menuDepthOrder = Map();
  int depthCount = 0;
  late MenuList menusList;
  late MenuItemsList menuItemsList;

  GetHierarchycalMenu();

  void _prepareMenuDepthOrder() {
    this.menusList.menus.forEach((menu) {
      if (!menuDepthOrder.containsKey(menu.depth)) {
        menuDepthOrder[menu.depth] = Map();
      }
      if (!menuDepthOrder[menu.depth]!.containsKey(menu.order)) {
        menuDepthOrder[menu.depth]![menu.order] = [];
      }
      menuDepthOrder[menu.depth]![menu.order]!.add(menu);
    });
  }

  void _sortMenuDepthOrder() {
    menuDepthOrder.forEach((key, value) {
      final sorted = new SplayTreeMap<int, List<Menu>>.from(
          value, (a, b) => a > b ? 1 : -1);
      menuDepthOrder[key] = sorted;
    });
    menuDepthOrder = new SplayTreeMap<int, Map<int, List<Menu>>>.from(
        menuDepthOrder, (a, b) => a > b ? 1 : -1);
  }

  void _sortMenuItemsList() {
    menuItemsList.menuItems.sort((a, b) => a.order! > b.order! ? 1 : -1);
  }

  void _fillMenuHierarchy() {
    _fillMenuHierarchyMenus();
    _fillMenuHierarchyMenuItems();
  }

  void _fillMenuHierarchyMenus() {
    menuDepthOrder.forEach((depth, menuOrder) {
      //Depths
      menuOrder.forEach((order, menus) {
        //Orders
        menus.forEach((menu) {
          final Either<bool, MenuHierarchy> menuHierarchyById =
              _getMenuHierarchyById(menu.parentId, rootMenuHierarchy);
          final MenuHierarchy localMenuHierarchy = MenuHierarchy();
          localMenuHierarchy.menu = menu;
          menuHierarchyById.fold((failure) {
            rootMenuHierarchy.nested!.add(localMenuHierarchy);
          }, (menuHierarchy) {
            menuHierarchy.nested!.add(localMenuHierarchy);
          });
        });
      });
    });
  }

  void _fillMenuHierarchyMenuItems() {
    menuItemsList.menuItems.forEach((menuItem) {
      final Either<bool, MenuHierarchy> menuHierarchyById =
          _getMenuHierarchyById(menuItem.menuId, rootMenuHierarchy);
      menuHierarchyById.fold((failure) {
        rootMenuHierarchy.menuItems!.add(menuItem);
      }, (menuHierarchy) {
        menuHierarchy.menuItems!.add(menuItem);
      });
    });
  }

  Either<bool, MenuHierarchy> _getMenuHierarchyById(
      String id, MenuHierarchy localMenuHierarchy) {
    if (localMenuHierarchy.menu!.id == id) {
      return Right(localMenuHierarchy);
    } else {
      if (localMenuHierarchy.nested is List<MenuHierarchy>) {
        Either<bool, MenuHierarchy> subMenuHierarchyOrBool = Left(false);
        localMenuHierarchy.nested!.forEach((element) {
          subMenuHierarchyOrBool = _getMenuHierarchyById(id, element).isRight()
              ? _getMenuHierarchyById(id, element)
              : subMenuHierarchyOrBool;
        });
        if (subMenuHierarchyOrBool.isRight()) {
          return subMenuHierarchyOrBool;
        } else {
          return Left(false);
        }
      } else {
        return Left(false);
      }
    }
  }

  @override
  Future<Either<Failure, MenuHierarchy>> call(ParamsHierarchy params) async {
    this.menusList = params.menusList;
    this.menuItemsList = params.menuItemsList;

    this.rootMenuHierarchy.menu = Menu(
      id: 'rootMenu',
      name: 'Root menu',
      description: '',
      parentId: 'undefined',
      order: -1,
      depth: -1,
    );
    rootMenuHierarchy.nested = [];
    menuDepthOrder = Map();
    _prepareMenuDepthOrder();
    _sortMenuDepthOrder();
    _sortMenuItemsList();
    _fillMenuHierarchy();
    return Future.value(Right(rootMenuHierarchy));
  }
}
