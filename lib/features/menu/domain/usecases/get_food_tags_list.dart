import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/repositories/menu_repository.dart';

class GetFoodTagsList implements UseCase<FoodTagsList, NoParams> {
  final MenuRepository repository;

  GetFoodTagsList(this.repository);

  @override
  Future<Either<Failure, FoodTagsList>> call(
    NoParams params,
  ) async {
    return await repository.getFoodTagsList();
  }
}
