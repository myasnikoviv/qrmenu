import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/repositories/menu_repository.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';

class GetMenuItemsList implements UseCase<MenuItemsList, Params> {
  final MenuRepository repository;

  GetMenuItemsList(this.repository);

  @override
  Future<Either<Failure, MenuItemsList>> call(
    Params params,
  ) async {
    return await repository.getMenuItemsList(params.restaurantId);
  }
}
