import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/repositories/menu_repository.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';

class GetMenusList implements UseCase<MenuList, Params> {
  final MenuRepository repository;

  GetMenusList(this.repository);

  @override
  Future<Either<Failure, MenuList>> call(
    Params params,
  ) async {
    return await repository.getMenusList(params.restaurantId);
  }
}
