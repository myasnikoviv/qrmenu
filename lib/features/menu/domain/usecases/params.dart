import 'package:equatable/equatable.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';

class Params extends Equatable {
  final String restaurantId;

  Params({required this.restaurantId});

  @override
  List<Object?> get props => [restaurantId];
}

class ParamsHierarchy extends Equatable {
  final MenuList menusList;
  final MenuItemsList menuItemsList;

  ParamsHierarchy({required this.menusList, required this.menuItemsList});

  @override
  List<Object?> get props => [menusList, menuItemsList];
}
