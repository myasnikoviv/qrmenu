import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/repositories/menu_repository.dart';

class GetMenuItemIconsList implements UseCase<MenuItemIconsList, NoParams> {
  final MenuRepository repository;

  GetMenuItemIconsList(this.repository);

  @override
  Future<Either<Failure, MenuItemIconsList>> call(
    NoParams params,
  ) async {
    return await repository.getMenuItemIconsList();
  }
}
