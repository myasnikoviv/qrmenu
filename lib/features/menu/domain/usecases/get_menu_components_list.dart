import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/repositories/menu_repository.dart';

class GetMenuComponentsList implements UseCase<MenuComponentsList, NoParams> {
  final MenuRepository repository;

  GetMenuComponentsList(this.repository);

  @override
  Future<Either<Failure, MenuComponentsList>> call(
    NoParams params,
  ) async {
    return await repository.getMenuComponentsList();
  }
}
