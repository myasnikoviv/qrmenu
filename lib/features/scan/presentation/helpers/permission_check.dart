import 'dart:io';

import 'package:permission_handler/permission_handler.dart';

class PermissionCheck {
  static Future<bool> canReadStorage() async {
    if (Platform.isIOS) return true;
    var status = await Permission.storage.status;
    if (status != PermissionStatus.granted) {
      var future = await Permission.storage.request();
      if (future != PermissionStatus.granted) {
        return false;
      }
    } else {
      return true;
    }
    return true;
  }

  static Future<bool> canOpenCamera() async {
    var status = await Permission.camera.status;
    if (status != PermissionStatus.granted) {
      var future = await Permission.camera.request();
      if (future != PermissionStatus.granted) {
        return false;
      }
    } else {
      return true;
    }
    return true;
  }
}
