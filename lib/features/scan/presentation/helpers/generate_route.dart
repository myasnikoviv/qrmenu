import 'package:dartz/dartz.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class GenerateRoute {
  static Either<bool, String> generate(Barcode barcode) {
    if (barcode.format != BarcodeFormat.qrcode) {
      return Left(false);
    }
    if (!barcode.code.contains('qrmenuapp.page.link')) {
      return Left(false);
    } else {
      return Right(barcode.code);
    }
  }
}
