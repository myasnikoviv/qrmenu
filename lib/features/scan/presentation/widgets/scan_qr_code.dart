import 'dart:io';

import 'package:dartz/dartz.dart' as d;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qrmenu/features/scan/presentation/helpers/generate_route.dart';
import 'package:url_launcher/url_launcher.dart';

class ScanQrCode extends StatefulWidget {
  const ScanQrCode({Key? key}) : super(key: key);

  @override
  _ScanQrCodeState createState() => _ScanQrCodeState();
}

class _ScanQrCodeState extends State<ScanQrCode> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late Barcode result;
  bool isInitiatedResult = false;
  bool isInitiatedController = false;
  late QRViewController controller;

  @override
  void reassemble() {
    super.reassemble();
    if (isInitiatedController) {
      if (Platform.isAndroid) {
        controller.pauseCamera();
      }
      controller.resumeCamera();
    }
  }

  @override
  void dispose() {
    if (isInitiatedController) {
      controller.dispose();
      super.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(flex: 4, child: _buildQrView(context)),
        Expanded(
          flex: 1,
          child: FittedBox(
            fit: BoxFit.contain,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                if (isInitiatedResult)
                  Text(
                      'Barcode Type: ${describeEnum(result.format)}   Data: ${result.code}')
                else
                  Text('Scan a code'),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(8),
                      child: isInitiatedController
                          ? FutureBuilder(
                              future: controller.getFlashStatus(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<dynamic> snapshot) {
                                Color iconColor = Colors.white;

                                if (snapshot.hasData) {
                                  if (snapshot.data == true) {
                                    iconColor = Colors.amber;
                                  }
                                }
                                return Ink(
                                  decoration: ShapeDecoration(
                                    shape: CircleBorder(),
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  child: IconButton(
                                    icon: const Icon(Icons.flash_on),
                                    color: iconColor,
                                    tooltip: 'Flip camera',
                                    onPressed: () async {
                                      await controller.toggleFlash();
                                      setState(() {});
                                    },
                                  ),
                                );
                              },
                            )
                          : Container(),
                    ),
                    Container(
                      margin: EdgeInsets.all(8),
                      child: isInitiatedController
                          ? Ink(
                              decoration: ShapeDecoration(
                                shape: CircleBorder(),
                                color: Theme.of(context).primaryColor,
                              ),
                              child: IconButton(
                                icon: const Icon(Icons.cameraswitch),
                                color: Colors.white,
                                tooltip: 'Flip camera',
                                onPressed: () async {
                                  await controller.flipCamera();
                                  setState(() {});
                                },
                              ),
                            )
                          : Container(),
                    )
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildQrView(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 300.0
        : 400.0;
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
        borderColor: Colors.red,
        borderRadius: 10,
        borderLength: 30,
        borderWidth: 10,
        cutOutSize: scanArea,
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
      isInitiatedController = true;
    });
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        if (scanData is Barcode) {
          isInitiatedResult = true;
          result = scanData;
          d.Either<bool, String> falseOrUrl = GenerateRoute.generate(scanData);
          falseOrUrl.fold((l) => null, (_url) async {
            // await Future.delayed(Duration(seconds: 3));
            // final PendingDynamicLinkData? data =
            // await FirebaseDynamicLinks.instance.getDynamicLink(Uri.parse(_url));
            // await Navigator.of(context)
            //     .pushNamed(data!.link.path, arguments: ScreenArguments('Menu'));
            await canLaunch(_url)
                ? await launch(_url)
                : throw 'Could not launch $_url';
          });
        }
      });
    });
  }
}
