import 'package:flutter/material.dart';
import 'package:qrmenu/core/routes/screen_arguments.dart';

class RefreshScanPageButton extends StatelessWidget {
  const RefreshScanPageButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Ink(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        ),
        child: IconButton(
          icon: const Icon(Icons.refresh),
          color: Colors.white,
          onPressed: () async {
            await Navigator.of(context)
                .pushNamed('/scan', arguments: ScreenArguments('Scan QR code'));
          },
        ));
  }
}
