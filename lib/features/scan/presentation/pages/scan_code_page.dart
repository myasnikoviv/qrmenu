import 'package:flutter/material.dart';
import 'package:qrmenu/core/presentation/pages/main_template.dart';
import 'package:qrmenu/features/scan/presentation/helpers/permission_check.dart';
import 'package:qrmenu/features/scan/presentation/pages/refresh_scan_page_button.dart';
import 'package:qrmenu/features/scan/presentation/widgets/scan_qr_code.dart';

class ScanCodePage extends StatefulWidget {
  const ScanCodePage({Key? key}) : super(key: key);

  @override
  _ScanCodePageState createState() => _ScanCodePageState();
}

class _ScanCodePageState extends State<ScanCodePage> {
  bool _canReadStoragePermissionStatus = false;
  bool _canOpenCameraPermissionStatus = false;

  @override
  void initState() {
    super.initState();
    _listenForPermissionStatus();
  }

  void _listenForPermissionStatus() async {
    final canReadStorageStatus = await PermissionCheck.canReadStorage();
    setState(() => _canReadStoragePermissionStatus = canReadStorageStatus);
    final canOpenCameraStatus = await PermissionCheck.canOpenCamera();
    setState(() => _canOpenCameraPermissionStatus = canOpenCameraStatus);
  }

  @override
  Widget build(BuildContext context) {
    return MainTemplate(
        child: !_canReadStoragePermissionStatus
            ? LoadingPermission(
                text: 'Пожалуйста, предоставьте доступ к хранилищу')
            : !_canOpenCameraPermissionStatus
                ? LoadingPermission(
                    text: 'Пожалуйста, предоставьте доступ к камере')
                : ScanQrCode());
  }
}

class LoadingPermission extends StatelessWidget {
  const LoadingPermission({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const CircularProgressIndicator(),
            const SizedBox(
              height: 10,
            ),
            Text(text),
            const SizedBox(
              height: 10,
            ),
            const RefreshScanPageButton(),
          ],
        ),
      ),
    );
  }
}
