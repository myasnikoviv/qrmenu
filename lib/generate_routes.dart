import 'package:flutter_deep_linking/flutter_deep_linking.dart' as dl;

import 'features/menu/presentation/bloc/blocs.dart';
import 'features/menu/presentation/bloc/food_tags/food_tags_list_bloc.dart';
import 'features/menu/presentation/bloc/menu/menu_bloc.dart';
import 'features/menu/presentation/bloc/menu_components/menu_components_list_bloc.dart';
import 'features/menu/presentation/bloc/menu_item_icons/menu_item_icons_list_bloc.dart';
import 'features/menu/presentation/pages/menu_page.dart';
import 'features/scan/presentation/pages/scan_code_page.dart';

class GenerateRoutes {
  static dl.Router generate() {
    Blocs blocs = Blocs();

    return dl.Router(
      routes: [
        dl.Route(
            matcher: dl.Matcher.path('scan'),
            materialBuilder: (_, dl.RouteResult result) {
              return ScanCodePage();
            }),
        dl.Route(
          matcher: dl.Matcher.path('menu'),
          materialBuilder: (_, dl.RouteResult result) {
            blocs.menu.add(GetMenuByRestaurantIdEvent('1'));
            return MenuPage();
          },
          routes: [
            dl.Route(
              matcher: dl.Matcher.path('{restaurantId}'),
              materialBuilder: (_, dl.RouteResult result) {
                String restaurantId = '1';
                if (result['restaurantId'] != null) {
                  restaurantId = result['restaurantId'].toString();
                }
                blocs.menu.add(GetMenuByRestaurantIdEvent(restaurantId));
                return MenuPage();
              },
            ),
          ],
        ),
        dl.Route(
          materialBuilder: (_, dl.RouteResult result) {
            blocs.foodTags.add(GetFoodTagsListEvent());
            blocs.icons.add(GetMenuItemIconsListEvent());
            blocs.components.add(GetMenuComponentsListEvent());
            return ScanCodePage();
          },
        ),
      ],
    );
  }
}
