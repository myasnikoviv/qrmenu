import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'build_theme_data.dart';
import 'core/dependencies/google_services.dart';
import 'generate_routes.dart';
import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final googleServices = GoogleServices();

  if (await googleServices.checkAvailability()) {
    await Firebase.initializeApp();
    await di.init();
    runApp(MyApp());
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'QR Menu app',
      theme: buildThemeData(),
      initialRoute: '/scan',
      onGenerateRoute: GenerateRoutes.generate().onGenerateRoute,
    );
  }
}
