import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qrmenu/core/network/network_info.dart';
import 'package:qrmenu/core/util/input_converter.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_local_data_source.dart';
import 'package:qrmenu/features/menu/data/repositories/menu_repository_implementation.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_food_tags_list.dart';
import 'package:qrmenu/features/menu/presentation/bloc/food_tags/food_tags_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu/menu_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_components/menu_components_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_item_icons/menu_item_icons_list_bloc.dart';

import 'features/menu/data/data_sources/menu_remote_data_source.dart';
import 'features/menu/domain/repositories/menu_repository.dart';
import 'features/menu/domain/usecases/get_hierarchycal_menu.dart';
import 'features/menu/domain/usecases/get_menu_components_list.dart';
import 'features/menu/domain/usecases/get_menu_item_icons_list.dart';
import 'features/menu/domain/usecases/get_menu_items_list.dart';
import 'features/menu/domain/usecases/get_menus_list.dart';

final sl = GetIt.instance;

Future init() async {
  final appDir = await getApplicationDocumentsDirectory();

  //? Features - Menu
  initMenuFeature();
  //? Core
  sl.registerLazySingleton(() => InputConverter());
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImplementation(sl()));
  //? External
  sl.registerLazySingleton<FirebaseFirestore>(() => FirebaseFirestore.instance);
  sl.registerLazySingleton<HiveInterface>(() => Hive);
  sl.registerLazySingleton(() => Connectivity());
  sl.registerLazySingleton<Directory>(() => appDir);

  return Future(() => true);
}

void initMenuFeature() {
  //? Bloc
  sl.registerFactory(
    () => MenuBloc(
      getHierarchicalMenu: sl(),
      getMenuItemsList: sl(),
      getMenusList: sl(),
      inputConverter: sl(),
    ),
  );
  sl.registerFactory(() => MenuItemIconsListBloc(getMenuItemIconsList: sl()));
  sl.registerFactory(() => FoodTagsListBloc(getFoodTagsList: sl()));
  sl.registerFactory(() => MenuComponentsListBloc(getMenuComponentsList: sl()));

  //? Use cases
  sl.registerLazySingleton(() => GetFoodTagsList(sl()));
  sl.registerLazySingleton(() => GetMenuComponentsList(sl()));
  sl.registerLazySingleton(() => GetMenuItemIconsList(sl()));
  sl.registerLazySingleton(() => GetMenuItemsList(sl()));
  sl.registerLazySingleton(() => GetMenusList(sl()));
  sl.registerLazySingleton(() => GetHierarchycalMenu());

  //? Repository
  sl.registerLazySingleton<MenuRepository>(
    () => MenuRepositoryImplementation(
      remoteDataSource: sl(),
      localDataSource: sl(),
      networkInfo: sl(),
    ),
  );

  //? Data sources
  sl.registerLazySingleton<MenuRemoteDataSource>(
      () => MenuRemoteDataSourceImplementation(firebaseFirestore: sl()));
  sl.registerLazySingleton<MenuLocalDataSource>(() =>
      MenuLocalDataSourceImplementation(hiveInterface: sl(), appDocDir: sl()));
}
