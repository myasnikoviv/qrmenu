import 'package:flutter/material.dart';

ThemeData buildThemeData() {
  return ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.lightBlue[800],
    accentColor: Colors.cyan[600],
    fontFamily: 'Roboto',
    textTheme: const TextTheme(
      headline1: const TextStyle(
          fontSize: 42, fontWeight: FontWeight.bold, color: Colors.black),
      headline2: const TextStyle(
        fontSize: 32,
        fontWeight: FontWeight.bold,
        color: Colors.white,
        height: 1,
      ),
      headline3: const TextStyle(
        fontSize: 32,
        fontWeight: FontWeight.bold,
        color: Colors.black,
        height: 1,
      ),
    ),
  );
}
