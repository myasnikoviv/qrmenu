import 'package:flutter/services.dart';
import 'package:google_api_availability/google_api_availability.dart';

class GoogleServices {
  GooglePlayServicesAvailability _availability =
      GooglePlayServicesAvailability.unknown;

  GoogleServices();

  Future checkAvailability() async {
    try {
      _availability = await GoogleApiAvailability.instance
          .checkGooglePlayServicesAvailability(true);
    } on PlatformException {
      _availability = GooglePlayServicesAvailability.unknown;
    }
    if (_availability == GooglePlayServicesAvailability.unknown) {
      return false;
    } else {
      return true;
    }
  }
}
