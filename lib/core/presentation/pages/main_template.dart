import 'package:flutter/material.dart';
import 'package:qrmenu/core/routes/screen_arguments.dart';
import 'package:qrmenu/features/menu/presentation/bloc/blocs.dart';
import 'package:qrmenu/features/menu/presentation/bloc/food_tags/food_tags_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_components/menu_components_list_bloc.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_item_icons/menu_item_icons_list_bloc.dart';

import '../../../deep_linking.dart';

class MainTemplate extends StatefulWidget {
  const MainTemplate({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  _MainTemplateState createState() => _MainTemplateState();
}

class _MainTemplateState extends State<MainTemplate> {
  @override
  void initState() {
    super.initState();
    DeepLinking deepLinking = DeepLinking();
    deepLinking.initDynamicLinks(context);
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments;
    final title =
        args == null ? 'Scan QR code' : (args as ScreenArguments).title;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: ModalRoute.of(context)!.settings.name != '/scan'
            ? [
                IconButton(
                  icon: const Icon(Icons.camera_alt_outlined),
                  tooltip: 'Scan QR code',
                  onPressed: () async {
                    await Navigator.of(context).pushNamed('/scan',
                        arguments: ScreenArguments('Scan QR code'));
                  },
                ),
              ]
            : [],
      ),
      body: SafeArea(child: widget.child),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Qrmenu options'),
            ),
            ListTile(
              leading: Icon(Icons.qr_code),
              title: const Text('Scan QR code'),
              onTap: () async {
                Navigator.pop(context);
                await Navigator.of(context).pushNamed('/scan',
                    arguments: ScreenArguments('Scan QR code'));
              },
            ),
            ListTile(
              leading: Icon(Icons.menu_book),
              title: const Text('Get a test menu'),
              onTap: () async {
                Navigator.pop(context);
                await Navigator.of(context)
                    .pushNamed('/menu/1', arguments: ScreenArguments('Menu 1'));
              },
            ),
            ListTile(
              leading: Icon(Icons.refresh),
              title: const Text('Refresh components and tags'),
              onTap: () {
                refresh(context);
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  void refresh(context) {
    final Blocs blocs = Blocs();
    blocs.foodTags.add(GetFoodTagsListEvent());
    blocs.icons.add(GetMenuItemIconsListEvent());
    blocs.components.add(GetMenuComponentsListEvent());
  }
}

