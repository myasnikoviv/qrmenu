import 'package:dartz/dartz.dart';
import 'package:qrmenu/core/error/failure.dart';

class InputConverter {
  Either<Failure, String> checkInputStringId(String str) {
    final RegExp regExp = new RegExp(
      r'^[a-zA-Z0-9]+$',
      caseSensitive: false,
      multiLine: false,
    );
    if (regExp.hasMatch(str)) {
      return Right(str);
    } else {
      return Left(InvalidInputFailure());
    }
  }
}

class InvalidInputFailure extends Failure {}
