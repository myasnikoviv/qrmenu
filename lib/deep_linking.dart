import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';

import 'core/routes/screen_arguments.dart';

class DeepLinking {
  void initDynamicLinks(BuildContext context) async {
    await Future.delayed(Duration(seconds: 3));
    final PendingDynamicLinkData? data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri? deepLink = data?.link;

    if (deepLink != null) {
      await Navigator.of(context)
          .pushNamed(deepLink.path, arguments: ScreenArguments('Menu'));
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData? dynamicLink) async {
      final Uri? deepLink = dynamicLink?.link;

      if (deepLink != null) {
        await Navigator.of(context)
            .pushNamed(deepLink.path, arguments: ScreenArguments('Menu'));
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }
}
