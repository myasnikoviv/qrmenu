import 'dart:convert';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_item_icons_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_icon_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_icon.dart';

void main() {
  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tIcon = '//path';

  final instance = FakeFirebaseFirestore();
  List<Map<String, dynamic>> tItemIconsList = [];

  final tItemIconsListModel = MenuItemIconsListModel(menuItemIcons: [
    MenuItemIconModel(
      id: uid1,
      icon: tIcon,
    ),
    MenuItemIconModel(
      id: uid2,
      icon: tIcon,
    ),
  ]);

  setUp(() async {
    await instance.collection('menu_icons').doc(uid1).set({
      'icon': tIcon,
    });
    await instance.collection('menu_icons').doc(uid2).set({
      'icon': tIcon,
    });
    final snapshot = await instance.collection('menu_icons').get();

    tItemIconsList = [];
    snapshot.docs.forEach((icon) {
      tItemIconsList.add({
        "id": icon.id,
        "icon": icon['icon'],
      });
    });
  });

  test('should be a subclass of menuItemIconsList entity', () async {
    expect(tItemIconsListModel, isA<MenuItemIconsList>());
    tItemIconsListModel.menuItemIcons.forEach((icon) {
      expect(icon, isA<MenuItemIcon>());
    });
  });

  group('fromJSON', () {
    test(
      'Should return a valid menuItemIconsList model from JSON',
      () async {
        // arrange
        final List<dynamic> tMenuItemIconsListJson =
            json.decode(jsonEncode(tItemIconsList));
        // act
        final result = MenuItemIconsListModel.fromJson(tMenuItemIconsListJson);
        // assert
        expect(result.menuItemIcons[0], tItemIconsListModel.menuItemIcons[0]);
        expect(result.menuItemIcons[1], tItemIconsListModel.menuItemIcons[1]);
        expect(result, tItemIconsListModel);
      },
    );
  });

  group('fromObj', () {
    test(
      'Should return a valid menuItemIconsList model from Object',
      () async {
        // arrange
        // act
        final result = MenuItemIconsListModel.fromObject(tItemIconsList);
        // assert
        expect(result.menuItemIcons[0], tItemIconsListModel.menuItemIcons[0]);
        expect(result.menuItemIcons[1], tItemIconsListModel.menuItemIcons[1]);
        expect(result, tItemIconsListModel);
      },
    );
  });
}
