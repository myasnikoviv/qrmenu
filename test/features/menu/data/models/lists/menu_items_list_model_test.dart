import 'dart:convert';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_items_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';

void main() {
  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test name';
  const String tDescription = 'Test description';
  const String tMenuId = '1';
  const double tPrice = 1.2;
  const int tOrder = 1;
  const String tPicture = '1';

  final instance = FakeFirebaseFirestore();
  List<Map<String, dynamic>> tMenuItemsList = [];
  List<String> tFoodTagsList = [uid1, uid2];
  List<String> tMenuComponents = [uid1, uid2];

  final tmenuItemsListModel = MenuItemsListModel(menuItems: [
    MenuItemModel(
      id: uid1,
      name: tName,
      description: tDescription,
      menuId: tMenuId,
      price: tPrice,
      order: tOrder,
      picture: tPicture,
      foodTags: tFoodTagsList,
      components: tMenuComponents,
    ),
    MenuItemModel(
      id: uid2,
      name: tName,
      description: tDescription,
      menuId: tMenuId,
      price: tPrice,
      order: tOrder,
      picture: tPicture,
      foodTags: tFoodTagsList,
      components: tMenuComponents,
    ),
  ]);

  setUp(() async {
    await instance.collection('menu_items').doc(uid1).set({
      'name': tName,
      'description': tDescription,
      'menuId': tMenuId,
      'price': tPrice,
      'order': tOrder,
      'picture': tPicture,
      'foodTags': tFoodTagsList,
      'components': tMenuComponents,
    });
    await instance.collection('menu_items').doc(uid2).set({
      'name': tName,
      'description': tDescription,
      'menuId': tMenuId,
      'price': tPrice,
      'order': tOrder,
      'picture': tPicture,
      'foodTags': tFoodTagsList,
      'components': tMenuComponents,
    });
    final snapshot = await instance.collection('menu_items').get();

    tMenuItemsList = [];
    snapshot.docs.forEach((menuItem) {
      tMenuItemsList.add({
        "id": menuItem.id,
        "name": menuItem['name'],
        "description": menuItem['description'],
        "menuId": menuItem['menuId'],
        "price": menuItem['price'],
        "order": menuItem['order'],
        "picture": menuItem['picture'],
        "foodTags": menuItem['foodTags'],
        "components": menuItem['components'],
      });
    });
  });

  test('should be a subclass of MenuComponentsList entity', () async {
    expect(tmenuItemsListModel, isA<MenuItemsList>());
    tmenuItemsListModel.menuItems.forEach((menuItem) {
      expect(menuItem, isA<MenuItem>());
    });
  });

  group('fromJSON', () {
    test(
      'Should return a valid MenuItemsList model from JSON',
      () async {
        // arrange
        final List<dynamic> tMenuItemIconsListJson =
            json.decode(jsonEncode(tMenuItemsList));
        // act
        final result = MenuItemsListModel.fromJson(tMenuItemIconsListJson);
        // assert
        expect(result.menuItems[0], tmenuItemsListModel.menuItems[0]);
        expect(result.menuItems[1], tmenuItemsListModel.menuItems[1]);
        expect(result, tmenuItemsListModel);
      },
    );
  });

  group('fromObj', () {
    test(
      'Should return a valid MenuItemsList model from Object',
      () async {
        // arrange
        // act
        final result = MenuItemsListModel.fromObject(tMenuItemsList);
        // assert
        expect(result.menuItems[0], tmenuItemsListModel.menuItems[0]);
        expect(result.menuItems[1], tmenuItemsListModel.menuItems[1]);
        expect(result, tmenuItemsListModel);
      },
    );
  });
}
