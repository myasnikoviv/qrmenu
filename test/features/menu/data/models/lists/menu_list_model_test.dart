import 'dart:convert';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';

void main() {
  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test name';
  const String tDescription = 'Test description';
  const String tParentId = '1';
  const int tOrder = 1;
  const int tDepth = 1;

  final instance = FakeFirebaseFirestore();
  List<Map<String, dynamic>> tMenusList = [];

  final tMenusListModel = MenuListModel(menus: [
    MenuModel(
      id: uid1,
      name: tName,
      description: tDescription,
      parentId: tParentId,
      order: tOrder,
      depth: tDepth,
    ),
    MenuModel(
      id: uid2,
      name: tName,
      description: tDescription,
      parentId: tParentId,
      order: tOrder,
      depth: tDepth,
    ),
  ]);

  setUp(() async {
    await instance.collection('menus').doc(uid1).set({
      'name': tName,
      'description': tDescription,
      'parentId': tParentId,
      'order': tOrder,
      'depth': tDepth,
    });
    await instance.collection('menus').doc(uid2).set({
      'name': tName,
      'description': tDescription,
      'parentId': tParentId,
      'order': tOrder,
      'depth': tDepth,
    });
    final snapshot = await instance.collection('menus').get();

    tMenusList = [];
    snapshot.docs.forEach((menu) {
      tMenusList.add({
        "id": menu.id,
        "name": menu['name'],
        "description": menu['description'],
        "parentId": menu['parentId'],
        "order": menu['order'],
        "depth": menu['depth'],
      });
    });
  });

  test('should be a subclass of MenuComponentsList entity', () async {
    expect(tMenusListModel, isA<MenuList>());
    tMenusListModel.menus.forEach((menu) {
      expect(menu, isA<Menu>());
    });
  });

  group('fromJSON', () {
    test(
      'Should return a valid MenuItemsList model from JSON',
      () async {
        // arrange
        final List<dynamic> tMenusListJson =
            json.decode(jsonEncode(tMenusList));
        // act
        final result = MenuListModel.fromJson(tMenusListJson);
        // assert
        expect(result.menus[0], tMenusListModel.menus[0]);
        expect(result.menus[1], tMenusListModel.menus[1]);
        expect(result, tMenusListModel);
      },
    );
  });

  group('fromObj', () {
    test(
      'Should return a valid MenuItemsList model from Object',
      () async {
        // arrange
        // act
        final result = MenuListModel.fromObject(tMenusList);
        // assert
        expect(result.menus[0], tMenusListModel.menus[0]);
        expect(result.menus[1], tMenusListModel.menus[1]);
        expect(result, tMenusListModel);
      },
    );
  });
}
