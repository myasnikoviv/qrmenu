import 'dart:convert';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_components_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_component_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_component.dart';

void main() {
  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test Component';

  final instance = FakeFirebaseFirestore();
  List<Map<String, dynamic>> tComponentsList = [];

  final tComponentsListModel = MenuComponentsListModel(components: [
    MenuItemComponentModel(
      id: uid1,
      name: tName,
    ),
    MenuItemComponentModel(
      id: uid2,
      name: tName,
    ),
  ]);

  setUp(() async {
    await instance.collection('menu_components').doc(uid1).set({
      'name': tName,
    });
    await instance.collection('menu_components').doc(uid2).set({
      'name': tName,
    });
    final snapshot = await instance.collection('menu_components').get();

    tComponentsList = [];
    snapshot.docs.forEach((component) {
      tComponentsList.add({
        "id": component.id,
        "name": component['name'],
      });
    });
  });

  test('should be a subclass of MenuComponentsList entity', () async {
    expect(tComponentsListModel, isA<MenuComponentsList>());
    tComponentsListModel.components.forEach((component) {
      expect(component, isA<MenuItemComponent>());
    });
  });

  group('fromJSON', () {
    test(
      'Should return a valid MenuComponentsList model from JSON',
      () async {
        // arrange
        final List<dynamic> tMenuComponentsListJson =
            json.decode(jsonEncode(tComponentsList));
        // act
        final result =
            MenuComponentsListModel.fromJson(tMenuComponentsListJson);
        // assert
        expect(result.components[0], tComponentsListModel.components[0]);
        expect(result.components[1], tComponentsListModel.components[1]);
        expect(result, tComponentsListModel);
      },
    );
  });

  group('fromObj', () {
    test(
      'Should return a valid MenuComponentsList model from Object',
      () async {
        // arrange
        // act
        final result = MenuComponentsListModel.fromObject(tComponentsList);
        // assert
        expect(result.components[0], tComponentsListModel.components[0]);
        expect(result.components[1], tComponentsListModel.components[1]);
        expect(result, tComponentsListModel);
      },
    );
  });
}
