import 'dart:convert';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/features/menu/data/models/lists/food_tags_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/food_tag_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/food_tag.dart';

void main() {
  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test Food tag';
  const String tMenuIconId = '1';

  final instance = FakeFirebaseFirestore();
  List<Map<String, dynamic>> tFoodTagsList = [];

  final tFoodTagsListModel = FoodTagsListModel(foodTagsList: [
    FoodTagModel(id: uid1, name: tName, menuIconId: tMenuIconId),
    FoodTagModel(id: uid2, name: tName, menuIconId: tMenuIconId),
  ]);

  setUp(() async {
    await instance
        .collection('foodTags')
        .doc(uid1)
        .set({'name': tName, 'menuIconId': tMenuIconId});
    await instance
        .collection('foodTags')
        .doc(uid2)
        .set({'name': tName, 'menuIconId': tMenuIconId});
    final snapshot = await instance.collection('foodTags').get();

    tFoodTagsList = [];
    snapshot.docs.forEach((foodTag) {
      tFoodTagsList.add({
        "id": foodTag.id,
        "name": foodTag['name'],
        "menuIconId": foodTag['menuIconId'],
      });
    });
  });

  test('should be a subclass of FooTagsList entity', () async {
    expect(tFoodTagsListModel, isA<FoodTagsList>());
    tFoodTagsListModel.foodTagsList.forEach((foodTag) {
      expect(foodTag, isA<FoodTag>());
    });
  });

  group('fromJSON', () {
    test(
      'Should return a valid FoodTagsList model from JSON',
      () async {
        // arrange
        final List<dynamic> tFoodTagsListJson =
            json.decode(jsonEncode(tFoodTagsList));
        // act
        final result = FoodTagsListModel.fromJson(tFoodTagsListJson);
        // assert
        expect(result.foodTagsList[0], tFoodTagsListModel.foodTagsList[0]);
        expect(result.foodTagsList[1], tFoodTagsListModel.foodTagsList[1]);
        expect(result, tFoodTagsListModel);
      },
    );
  });

  group('fromObj', () {
    test(
      'Should return a valid FoodTagsList model from Object',
      () async {
        // arrange
        // act
        final result = FoodTagsListModel.fromObject(tFoodTagsList);
        // assert
        expect(result.foodTagsList[0], tFoodTagsListModel.foodTagsList[0]);
        expect(result.foodTagsList[1], tFoodTagsListModel.foodTagsList[1]);
        expect(result, tFoodTagsListModel);
      },
    );
  });
}
