import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_items_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_model.dart';
import 'package:qrmenu/features/menu/data/repositories/menu_repository_implementation.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late MenuRepositoryImplementation repository;
  late MockMenuRemoteDataSource mockMenuRemoteDataSource;
  late MockMenuLocalDataSource mockMenuLocalDataSource;
  late MockNetworkInfo mockNetworkInfo;

  late MenuItemsListModel tMenuItemsListModel;
  late MenuItemsList tMenuItemsList;

  late List<String> tFoodTagsList;
  late List<String> tMenuComponents;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test name';
  const String tDescription = 'Test description';
  const String tMenuId = '1';
  const double tPrice = 1.2;
  const int tOrder = 1;
  const String tPicture = '1';
  const String tRestaurantId = '1';

  setUp(() async {
    mockMenuRemoteDataSource = MockMenuRemoteDataSource();
    mockMenuLocalDataSource = MockMenuLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = MenuRepositoryImplementation(
      localDataSource: mockMenuLocalDataSource,
      remoteDataSource: mockMenuRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );

    tFoodTagsList = [uid1, uid2];
    tMenuComponents = [uid1, uid2];

    tMenuItemsListModel = MenuItemsListModel(menuItems: [
      MenuItemModel(
        id: uid1,
        name: tName,
        description: tDescription,
        menuId: tMenuId,
        price: tPrice,
        order: tOrder,
        picture: tPicture,
        foodTags: tFoodTagsList,
        components: tMenuComponents,
      ),
      MenuItemModel(
        id: uid2,
        name: tName,
        description: tDescription,
        menuId: tMenuId,
        price: tPrice,
        order: tOrder,
        picture: tPicture,
        foodTags: tFoodTagsList,
        components: tMenuComponents,
      ),
    ]);
    tMenuItemsList = tMenuItemsListModel;
  });

  group('getMenuItemsList', () {
    test(
      'Should check if the device is online or offline',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        when(mockMenuRemoteDataSource.getMenuItemsList(tRestaurantId))
            .thenAnswer((_) async => tMenuItemsListModel);
        // act
        await repository.getMenuItemsList(tRestaurantId);
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'should return remote data when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuItemsList(tRestaurantId))
              .thenAnswer((_) async => tMenuItemsListModel);
          // act
          final result = await repository.getMenuItemsList(tRestaurantId);
          // assert
          verify(mockMenuRemoteDataSource.getMenuItemsList(tRestaurantId));
          expect(result, equals(Right(tMenuItemsList)));
        },
      );
      test(
        'should cache data locally when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuItemsList(tRestaurantId))
              .thenAnswer((_) async => tMenuItemsListModel);
          // act
          await repository.getMenuItemsList(tRestaurantId);
          // assert
          verify(mockMenuRemoteDataSource.getMenuItemsList(tRestaurantId));
          verify(
              mockMenuLocalDataSource.cacheMenuItemsList(tRestaurantId, any));
        },
      );
      test(
        'should return server failure when the call to remote data source is unsuccessful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuItemsList(tRestaurantId))
              .thenThrow(ServerException());
          // act
          final result = await repository.getMenuItemsList(tRestaurantId);
          // assert
          verify(mockMenuRemoteDataSource.getMenuItemsList(tRestaurantId));
          expect(result, equals(Left(ServerFailure())));
          verifyNever(mockMenuLocalDataSource.getMenuItemsList(tRestaurantId));
        },
      );
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test(
        'should return locally cached data when the cached data is present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenuItemsList(tRestaurantId))
              .thenAnswer((_) async => tMenuItemsListModel);
          // act
          final result = await repository.getMenuItemsList(tRestaurantId);
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenuItemsList(tRestaurantId));
          expect(result, equals(Right(tMenuItemsList)));
        },
      );
      test(
        'should return CacheFailure data when the there is no cached data present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenuItemsList(tRestaurantId))
              .thenThrow(CacheException());
          // act
          final result = await repository.getMenuItemsList(tRestaurantId);
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenuItemsList(tRestaurantId));
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}
