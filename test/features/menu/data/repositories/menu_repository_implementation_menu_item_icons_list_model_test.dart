import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_item_icons_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_icon_model.dart';
import 'package:qrmenu/features/menu/data/repositories/menu_repository_implementation.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late MenuRepositoryImplementation repository;
  late MockMenuRemoteDataSource mockMenuRemoteDataSource;
  late MockMenuLocalDataSource mockMenuLocalDataSource;
  late MockNetworkInfo mockNetworkInfo;

  late MenuItemIconsListModel tMenuItemIconsListModel;
  late MenuItemIconsList tMenuItemIconsList;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tIcon = '//path';

  setUp(() async {
    mockMenuRemoteDataSource = MockMenuRemoteDataSource();
    mockMenuLocalDataSource = MockMenuLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = MenuRepositoryImplementation(
      localDataSource: mockMenuLocalDataSource,
      remoteDataSource: mockMenuRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );

    tMenuItemIconsListModel = MenuItemIconsListModel(menuItemIcons: [
      MenuItemIconModel(
        id: uid1,
        icon: tIcon,
      ),
      MenuItemIconModel(
        id: uid2,
        icon: tIcon,
      ),
    ]);
    tMenuItemIconsList = tMenuItemIconsListModel;
  });

  group('getMenuItemIconsList', () {
    test(
      'Should check if the device is online or offline',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        when(mockMenuRemoteDataSource.getMenuItemIconsList())
            .thenAnswer((_) async => tMenuItemIconsListModel);
        // act
        await repository.getMenuItemIconsList();
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'should return remote data when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuItemIconsList())
              .thenAnswer((_) async => tMenuItemIconsListModel);
          // act
          final result = await repository.getMenuItemIconsList();
          // assert
          verify(mockMenuRemoteDataSource.getMenuItemIconsList());
          expect(result, equals(Right(tMenuItemIconsList)));
        },
      );
      test(
        'should cache data locally when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuItemIconsList())
              .thenAnswer((_) async => tMenuItemIconsListModel);
          // act
          await repository.getMenuItemIconsList();
          // assert
          verify(mockMenuRemoteDataSource.getMenuItemIconsList());
          verify(mockMenuLocalDataSource.cacheMenuItemIconsList(any));
        },
      );
      test(
        'should return server failure when the call to remote data source is unsuccessful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuItemIconsList())
              .thenThrow(ServerException());
          // act
          final result = await repository.getMenuItemIconsList();
          // assert
          verify(mockMenuRemoteDataSource.getMenuItemIconsList());
          expect(result, equals(Left(ServerFailure())));
          verifyNever(mockMenuLocalDataSource.getMenuItemIconsList());
        },
      );
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test(
        'should return locally cached data when the cached data is present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenuItemIconsList())
              .thenAnswer((_) async => tMenuItemIconsListModel);
          // act
          final result = await repository.getMenuItemIconsList();
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenuItemIconsList());
          expect(result, equals(Right(tMenuItemIconsList)));
        },
      );
      test(
        'should return CacheFailure data when the there is no cached data present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenuItemIconsList())
              .thenThrow(CacheException());
          // act
          final result = await repository.getMenuItemIconsList();
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenuItemIconsList());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}
