import 'package:mockito/annotations.dart';
import 'package:qrmenu/core/network/network_info.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_local_data_source.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_remote_data_source.dart';

@GenerateMocks([MenuRemoteDataSource])
@GenerateMocks([MenuLocalDataSource])
@GenerateMocks([NetworkInfo])
void main() {}
