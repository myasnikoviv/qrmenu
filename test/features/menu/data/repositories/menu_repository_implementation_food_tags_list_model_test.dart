import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/data/models/lists/food_tags_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/food_tag_model.dart';
import 'package:qrmenu/features/menu/data/repositories/menu_repository_implementation.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late MenuRepositoryImplementation repository;
  late MockMenuRemoteDataSource mockMenuRemoteDataSource;
  late MockMenuLocalDataSource mockMenuLocalDataSource;
  late MockNetworkInfo mockNetworkInfo;
  late FoodTagModel tFoodTagModel;
  late FoodTagsListModel tFoodTagsListModel;
  late FoodTagsList tFoodTagsList;

  setUp(() async {
    mockMenuRemoteDataSource = MockMenuRemoteDataSource();
    mockMenuLocalDataSource = MockMenuLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = MenuRepositoryImplementation(
      localDataSource: mockMenuLocalDataSource,
      remoteDataSource: mockMenuRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );

    tFoodTagModel = FoodTagModel(
      name: 'Test tag',
      id: '1',
      menuIconId: '1',
    );
    tFoodTagsListModel = FoodTagsListModel(foodTagsList: [tFoodTagModel]);
    tFoodTagsList = tFoodTagsListModel;
  });

  group('getFoodTagsList', () {
    test(
      'Should check if the device is online or offline',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        when(mockMenuRemoteDataSource.getFoodTagsList())
            .thenAnswer((_) async => tFoodTagsListModel);
        // act
        await repository.getFoodTagsList();
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'should return remote data when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getFoodTagsList())
              .thenAnswer((_) async => tFoodTagsListModel);
          // act
          final result = await repository.getFoodTagsList();
          // assert
          verify(mockMenuRemoteDataSource.getFoodTagsList());
          expect(result, equals(Right(tFoodTagsList)));
        },
      );
      test(
        'should cache data locally when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getFoodTagsList())
              .thenAnswer((_) async => tFoodTagsListModel);
          // act
          final foodTagsListToCache = await repository.getFoodTagsList();
          // assert
          verify(mockMenuRemoteDataSource.getFoodTagsList());
          verify(mockMenuLocalDataSource.cacheFoodTagsList(any));
        },
      );
      test(
        'should return server failure when the call to remote data source is unsuccessful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getFoodTagsList())
              .thenThrow(ServerException());
          // act
          final result = await repository.getFoodTagsList();
          // assert
          verify(mockMenuRemoteDataSource.getFoodTagsList());
          expect(result, equals(Left(ServerFailure())));
          verifyNever(mockMenuLocalDataSource.getFoodTagsList());
        },
      );
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test(
        'should return locally cached data when the cached data is present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getFoodTagsList())
              .thenAnswer((_) async => tFoodTagsListModel);
          // act
          final result = await repository.getFoodTagsList();
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getFoodTagsList());
          expect(result, equals(Right(tFoodTagsList)));
        },
      );
      test(
        'should return CacheFailure data when the there is no cached data present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getFoodTagsList())
              .thenThrow(CacheException());
          // act
          final result = await repository.getFoodTagsList();
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getFoodTagsList());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}
