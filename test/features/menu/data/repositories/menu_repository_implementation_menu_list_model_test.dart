import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_model.dart';
import 'package:qrmenu/features/menu/data/repositories/menu_repository_implementation.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late MenuRepositoryImplementation repository;
  late MockMenuRemoteDataSource mockMenuRemoteDataSource;
  late MockMenuLocalDataSource mockMenuLocalDataSource;
  late MockNetworkInfo mockNetworkInfo;

  late MenuListModel tMenuListModel;
  late MenuList tMenuList;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test name';
  const String tDescription = 'Test description';
  const String tParentId = '1';
  const int tOrder = 1;
  const int tDepth = 1;
  const String tRestaurantId = '1';

  setUp(() async {
    mockMenuRemoteDataSource = MockMenuRemoteDataSource();
    mockMenuLocalDataSource = MockMenuLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = MenuRepositoryImplementation(
      localDataSource: mockMenuLocalDataSource,
      remoteDataSource: mockMenuRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );

    tMenuListModel = MenuListModel(menus: [
      MenuModel(
        id: uid1,
        name: tName,
        description: tDescription,
        parentId: tParentId,
        order: tOrder,
        depth: tDepth,
      ),
      MenuModel(
        id: uid2,
        name: tName,
        description: tDescription,
        parentId: tParentId,
        order: tOrder,
        depth: tDepth,
      ),
    ]);
    tMenuList = tMenuListModel;
  });

  group('getMenusList', () {
    test(
      'Should check if the device is online or offline',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        when(mockMenuRemoteDataSource.getMenusList(tRestaurantId))
            .thenAnswer((_) async => tMenuListModel);
        // act
        await repository.getMenusList(tRestaurantId);
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'should return remote data when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenusList(tRestaurantId))
              .thenAnswer((_) async => tMenuListModel);
          // act
          final result = await repository.getMenusList(tRestaurantId);
          // assert
          verify(mockMenuRemoteDataSource.getMenusList(tRestaurantId));
          expect(result, equals(Right(tMenuList)));
        },
      );
      test(
        'should cache data locally when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenusList(tRestaurantId))
              .thenAnswer((_) async => tMenuListModel);
          // act
          await repository.getMenusList(tRestaurantId);
          // assert
          verify(mockMenuRemoteDataSource.getMenusList(tRestaurantId));
          verify(mockMenuLocalDataSource.cacheMenusList(tRestaurantId, any));
        },
      );
      test(
        'should return server failure when the call to remote data source is unsuccessful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenusList(tRestaurantId))
              .thenThrow(ServerException());
          // act
          final result = await repository.getMenusList(tRestaurantId);
          // assert
          verify(mockMenuRemoteDataSource.getMenusList(tRestaurantId));
          expect(result, equals(Left(ServerFailure())));
          verifyNever(mockMenuLocalDataSource.getMenusList(tRestaurantId));
        },
      );
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test(
        'should return locally cached data when the cached data is present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenusList(tRestaurantId))
              .thenAnswer((_) async => tMenuListModel);
          // act
          final result = await repository.getMenusList(tRestaurantId);
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenusList(tRestaurantId));
          expect(result, equals(Right(tMenuList)));
        },
      );
      test(
        'should return CacheFailure data when the there is no cached data present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenusList(tRestaurantId))
              .thenThrow(CacheException());
          // act
          final result = await repository.getMenusList(tRestaurantId);
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenusList(tRestaurantId));
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}
