import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_components_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_component_model.dart';
import 'package:qrmenu/features/menu/data/repositories/menu_repository_implementation.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late MenuRepositoryImplementation repository;
  late MockMenuRemoteDataSource mockMenuRemoteDataSource;
  late MockMenuLocalDataSource mockMenuLocalDataSource;
  late MockNetworkInfo mockNetworkInfo;

  late MenuComponentsListModel tMenuComponentsListModel;
  late MenuComponentsList tMenuComponentsList;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test Component';

  setUp(() async {
    mockMenuRemoteDataSource = MockMenuRemoteDataSource();
    mockMenuLocalDataSource = MockMenuLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = MenuRepositoryImplementation(
      localDataSource: mockMenuLocalDataSource,
      remoteDataSource: mockMenuRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );

    tMenuComponentsListModel = MenuComponentsListModel(components: [
      MenuItemComponentModel(
        id: uid1,
        name: tName,
      ),
      MenuItemComponentModel(
        id: uid2,
        name: tName,
      ),
    ]);
    tMenuComponentsList = tMenuComponentsListModel;
  });

  group('getMenuComponentsList', () {
    test(
      'Should check if the device is online or offline',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        when(mockMenuRemoteDataSource.getMenuComponentsList())
            .thenAnswer((_) async => tMenuComponentsListModel);
        // act
        await repository.getMenuComponentsList();
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'should return remote data when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuComponentsList())
              .thenAnswer((_) async => tMenuComponentsListModel);
          // act
          final result = await repository.getMenuComponentsList();
          // assert
          verify(mockMenuRemoteDataSource.getMenuComponentsList());
          expect(result, equals(Right(tMenuComponentsList)));
        },
      );
      test(
        'should cache data locally when the call to remote data source is successful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuComponentsList())
              .thenAnswer((_) async => tMenuComponentsListModel);
          // act
          await repository.getMenuComponentsList();
          // assert
          verify(mockMenuRemoteDataSource.getMenuComponentsList());
          verify(mockMenuLocalDataSource.cacheMenuComponentsList(any));
        },
      );
      test(
        'should return server failure when the call to remote data source is unsuccessful',
        () async {
          // arrange
          when(mockMenuRemoteDataSource.getMenuComponentsList())
              .thenThrow(ServerException());
          // act
          final result = await repository.getMenuComponentsList();
          // assert
          verify(mockMenuRemoteDataSource.getMenuComponentsList());
          expect(result, equals(Left(ServerFailure())));
          verifyNever(mockMenuLocalDataSource.getMenuComponentsList());
        },
      );
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test(
        'should return locally cached data when the cached data is present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenuComponentsList())
              .thenAnswer((_) async => tMenuComponentsListModel);
          // act
          final result = await repository.getMenuComponentsList();
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenuComponentsList());
          expect(result, equals(Right(tMenuComponentsList)));
        },
      );
      test(
        'should return CacheFailure data when the there is no cached data present',
        () async {
          // arrange
          when(mockMenuLocalDataSource.getMenuComponentsList())
              .thenThrow(CacheException());
          // act
          final result = await repository.getMenuComponentsList();
          // assert
          verifyZeroInteractions(mockMenuRemoteDataSource);
          verify(mockMenuLocalDataSource.getMenuComponentsList());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}
