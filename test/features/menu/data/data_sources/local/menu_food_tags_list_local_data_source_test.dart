import 'dart:io';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_local_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/food_tags_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/food_tag_model.dart';

import 'FakeProviderPlatform.dart';
import 'generate_mocks.mocks.dart';

void main() {
  late MenuLocalDataSourceImplementation localDataSource;
  late MockHiveInterface mockHiveInterface;
  late MockBox mockBox;
  late List<Map<String, dynamic>> tFoodTagsList;
  late FoodTagsListModel tFoodTagsListEntity;
  late Directory appDocDir;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test Food tag';
  const String tMenuIconId = '1';

  setUp(() async {
    mockHiveInterface = MockHiveInterface();
    mockBox = MockBox();

    TestWidgetsFlutterBinding.ensureInitialized();
    PathProviderPlatform.instance = FakePathProviderPlatform();

    appDocDir = await getApplicationDocumentsDirectory();

    localDataSource = MenuLocalDataSourceImplementation(
        hiveInterface: mockHiveInterface, appDocDir: appDocDir);
    final instance = FakeFirebaseFirestore();
    tFoodTagsList = [];

    await instance
        .collection('foodTags')
        .doc(uid1)
        .set({'name': tName, 'menuIconId': tMenuIconId});
    await instance
        .collection('foodTags')
        .doc(uid2)
        .set({'name': tName, 'menuIconId': tMenuIconId});
    final snapshot = await instance.collection('foodTags').get();

    snapshot.docs.forEach((foodTag) {
      tFoodTagsList.add({
        "id": foodTag.id,
        "name": foodTag['name'],
        "menuIconId": foodTag['menuIconId'],
      });
    });
    tFoodTagsListEntity = FoodTagsListModel(foodTagsList: [
      FoodTagModel(id: uid1, name: tName, menuIconId: tMenuIconId),
      FoodTagModel(id: uid2, name: tName, menuIconId: tMenuIconId),
    ]);
  });

  group('getFoodTagsList', () {
    test(
      'should return FoodTagsList when there is a data stored',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => true);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        when(mockBox.values).thenAnswer((_) => tFoodTagsList);

        // act
        final result = await localDataSource.getFoodTagsList();
        // assert
        verify(mockHiveInterface.boxExists(any));
        verify(mockBox.values);
        expect(result, equals(tFoodTagsListEntity));
      },
    );
    test(
      'should throw CacheException when there is no box exist',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => false);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        // act
        final call = localDataSource.getFoodTagsList;
        // assert
        expect(() => call(), throwsA(TypeMatcher<CacheException>()));
        verify(mockHiveInterface.boxExists(any));
        verifyNever(mockBox.values);
      },
    );
  });
  group('cacheFoodTagsList', () {
    test(
      'should cache FoodTagsList when get it from remote source',
      () async {
        // arrange
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        // act
        await localDataSource.cacheFoodTagsList(tFoodTagsListEntity);
        // assert
        verify(mockBox.putAll(any));
      },
    );
  });
}
