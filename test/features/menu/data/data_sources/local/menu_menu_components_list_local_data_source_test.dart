import 'dart:io';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_local_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_components_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_component_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';

import 'FakeProviderPlatform.dart';
import 'generate_mocks.mocks.dart';

void main() {
  late MenuLocalDataSourceImplementation localDataSource;
  late MockHiveInterface mockHiveInterface;
  late MockBox mockBox;
  late Directory appDocDir;

  late List<Map<String, dynamic>> tComponentsList;
  late MenuComponentsList tMenuComponentsListEntity;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test Component';

  setUp(() async {
    mockHiveInterface = MockHiveInterface();
    mockBox = MockBox();

    TestWidgetsFlutterBinding.ensureInitialized();
    PathProviderPlatform.instance = FakePathProviderPlatform();

    appDocDir = await getApplicationDocumentsDirectory();
    localDataSource = MenuLocalDataSourceImplementation(
        hiveInterface: mockHiveInterface, appDocDir: appDocDir);
    final instance = FakeFirebaseFirestore();
    tComponentsList = [];

    await instance.collection('menu_components').doc(uid1).set({'name': tName});
    await instance.collection('menu_components').doc(uid2).set({'name': tName});
    final snapshot = await instance.collection('menu_components').get();

    snapshot.docs.forEach((menuComponent) {
      tComponentsList.add({
        "id": menuComponent.id,
        "name": menuComponent['name'],
      });
    });
    tMenuComponentsListEntity = MenuComponentsListModel(components: [
      MenuItemComponentModel(id: uid1, name: tName),
      MenuItemComponentModel(id: uid2, name: tName),
    ]);
  });

  group('getMenuComponentsList', () {
    test(
      'should return MenuComponentsList when there is a data stored',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => true);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        when(mockBox.values).thenAnswer((_) => tComponentsList);

        // act
        final result = await localDataSource.getMenuComponentsList();
        // assert
        verify(mockHiveInterface.boxExists(any));
        verify(mockBox.values);
        expect(result, equals(tMenuComponentsListEntity));
      },
    );
    test(
      'should throw CacheException when there is no box exist',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => false);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        // act
        final call = localDataSource.getMenuComponentsList;
        // assert
        expect(() => call(), throwsA(TypeMatcher<CacheException>()));
        verify(mockHiveInterface.boxExists(any));
        verifyNever(mockBox.values);
      },
    );
  });
  group('cacheMenuComponentsList', () {
    test(
      'should cache MenuComponentsList when get it from remote source',
      () async {
        // arrange
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        // act
        await localDataSource
            .cacheMenuComponentsList(tMenuComponentsListEntity);
        // assert
        verify(mockBox.putAll(any));
      },
    );
  });
}
