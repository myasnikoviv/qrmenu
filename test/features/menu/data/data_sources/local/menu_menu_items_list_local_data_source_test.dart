import 'dart:io';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_local_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_items_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';

import 'FakeProviderPlatform.dart';
import 'generate_mocks.mocks.dart';

void main() {
  late MenuLocalDataSourceImplementation localDataSource;
  late MockHiveInterface mockHiveInterface;
  late MockBox mockBox;
  late Directory appDocDir;

  late List<Map<String, dynamic>> tMenuItemsList;
  late MenuItemsList tMenuItemsListEntity;
  late List<String> tFoodTagsList;
  late List<String> tMenuComponents;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test name';
  const String tDescription = 'Test description';
  const String tMenuId = '1';
  const double tPrice = 1.2;
  const int tOrder = 1;
  const String tPicture = '1';
  const String tRestaurantId = '1';

  setUp(() async {
    mockHiveInterface = MockHiveInterface();
    mockBox = MockBox();

    TestWidgetsFlutterBinding.ensureInitialized();
    PathProviderPlatform.instance = FakePathProviderPlatform();

    appDocDir = await getApplicationDocumentsDirectory();
    localDataSource = MenuLocalDataSourceImplementation(
        hiveInterface: mockHiveInterface, appDocDir: appDocDir);
    final instance = FakeFirebaseFirestore();
    tMenuItemsList = [];
    tFoodTagsList = [uid1, uid2];
    tMenuComponents = [uid1, uid2];

    await instance.collection('menu_items').doc(uid1).set({
      'name': tName,
      'description': tDescription,
      'menuId': tMenuId,
      'price': tPrice,
      'order': tOrder,
      'picture': tPicture,
      'foodTags': tFoodTagsList,
      'components': tMenuComponents,
    });
    await instance.collection('menu_items').doc(uid2).set({
      'name': tName,
      'description': tDescription,
      'menuId': tMenuId,
      'price': tPrice,
      'order': tOrder,
      'picture': tPicture,
      'foodTags': tFoodTagsList,
      'components': tMenuComponents,
    });
    final snapshot = await instance.collection('menu_items').get();

    snapshot.docs.forEach((menuItem) {
      tMenuItemsList.add({
        "id": menuItem.id,
        "name": menuItem['name'],
        "description": menuItem['description'],
        "menuId": menuItem['menuId'],
        "price": menuItem['price'],
        "order": menuItem['order'],
        "picture": menuItem['picture'],
        "foodTags": menuItem['foodTags'],
        "components": menuItem['components'],
      });
    });
    tMenuItemsListEntity = MenuItemsListModel(menuItems: [
      MenuItemModel(
        id: uid1,
        name: tName,
        description: tDescription,
        menuId: tMenuId,
        price: tPrice,
        order: tOrder,
        picture: tPicture,
        foodTags: tFoodTagsList,
        components: tMenuComponents,
      ),
      MenuItemModel(
        id: uid2,
        name: tName,
        description: tDescription,
        menuId: tMenuId,
        price: tPrice,
        order: tOrder,
        picture: tPicture,
        foodTags: tFoodTagsList,
        components: tMenuComponents,
      ),
    ]);
  });

  group('getMenuItemsList', () {
    test(
      'should return MenuItemsList when there is a data stored',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => true);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        when(mockBox.values).thenAnswer((_) => tMenuItemsList);
        when(mockBox.get(any)).thenAnswer((_) => mockBox);

        // act
        final result = await localDataSource.getMenuItemsList(tRestaurantId);
        // assert
        verify(mockHiveInterface.boxExists(any));
        verify(mockBox.values);
        expect(result, equals(tMenuItemsListEntity));
      },
    );
    test(
      'should throw CacheException when there is no box exist',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => false);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        // act
        final call = localDataSource.getMenuItemsList;
        // assert
        expect(
            () => call(tRestaurantId), throwsA(TypeMatcher<CacheException>()));
        verify(mockHiveInterface.boxExists(any));
        verifyNever(mockBox.values);
      },
    );
  });
  group('cacheMenuItemsList', () {
    test(
      'should cache MenuItemsList when get it from remote source',
      () async {
        // arrange
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        when(mockBox.get(any)).thenAnswer((_) => mockBox);
        // act
        await localDataSource.cacheMenuItemsList(
            tRestaurantId, tMenuItemsListEntity);
        // assert
        verify(mockBox.put(any, any)).called(2);
      },
    );
  });
}
