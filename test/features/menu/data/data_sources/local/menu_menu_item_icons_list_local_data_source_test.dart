import 'dart:io';

import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_local_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_item_icons_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_icon_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';

import 'FakeProviderPlatform.dart';
import 'generate_mocks.mocks.dart';

void main() {
  late MenuLocalDataSourceImplementation localDataSource;
  late MockHiveInterface mockHiveInterface;
  late MockBox mockBox;
  late Directory appDocDir;

  late List<Map<String, dynamic>> tMenuItemIconsList;
  late MenuItemIconsList tMenuItemIconsListEntity;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tIcon = '//path';

  setUp(() async {
    mockHiveInterface = MockHiveInterface();
    mockBox = MockBox();

    TestWidgetsFlutterBinding.ensureInitialized();
    PathProviderPlatform.instance = FakePathProviderPlatform();

    appDocDir = await getApplicationDocumentsDirectory();
    localDataSource = MenuLocalDataSourceImplementation(
        hiveInterface: mockHiveInterface, appDocDir: appDocDir);
    final instance = FakeFirebaseFirestore();
    tMenuItemIconsList = [];

    await instance.collection('menu_icons').doc(uid1).set({'icon': tIcon});
    await instance.collection('menu_icons').doc(uid2).set({'icon': tIcon});
    final snapshot = await instance.collection('menu_icons').get();

    snapshot.docs.forEach((menuItemIcon) {
      tMenuItemIconsList.add({
        "id": menuItemIcon.id,
        "icon": menuItemIcon['icon'],
      });
    });
    tMenuItemIconsListEntity = MenuItemIconsListModel(menuItemIcons: [
      MenuItemIconModel(id: uid1, icon: tIcon),
      MenuItemIconModel(id: uid2, icon: tIcon),
    ]);
  });

  group('getMenuItemIconsList', () {
    test(
      'should return MenuItemIconsList when there is a data stored',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => true);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        when(mockBox.values).thenAnswer((_) => tMenuItemIconsList);

        // act
        final result = await localDataSource.getMenuItemIconsList();
        // assert
        verify(mockHiveInterface.boxExists(any));
        verify(mockBox.values);
        expect(result, equals(tMenuItemIconsListEntity));
      },
    );
    test(
      'should throw CacheException when there is no box exist',
      () async {
        // arrange
        when(mockHiveInterface.boxExists(any)).thenAnswer((_) async => false);
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        // act
        final call = localDataSource.getMenuItemIconsList;
        // assert
        expect(() => call(), throwsA(TypeMatcher<CacheException>()));
        verify(mockHiveInterface.boxExists(any));
        verifyNever(mockBox.values);
      },
    );
  });
  group('cacheMenuItemIconsList', () {
    test(
      'should cache MenuComponentsList when get it from remote source',
      () async {
        // arrange
        when(mockHiveInterface.openBox(any)).thenAnswer((_) async => mockBox);
        when(mockHiveInterface.box(any)).thenAnswer((_) => mockBox);
        // act
        await localDataSource.cacheMenuItemIconsList(tMenuItemIconsListEntity);
        // assert
        verify(mockBox.putAll(any));
      },
    );
  });
}
