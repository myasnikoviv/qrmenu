import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_remote_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_items_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';

void main() {
  late MenuRemoteDataSourceImplementation remoteDataSource;
  late MenuRemoteDataSourceImplementation remoteEmptyDataSource;
  late MenuRemoteDataSourceImplementation remoteWrongDataSource;

  late List<Map<String, dynamic>> tMenuItemsList;
  late MenuItemsList tMenuItemsListEntity;
  late List<String> tFoodTagsList;
  late List<String> tMenuComponents;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test name';
  const String tDescription = 'Test description';
  const String tMenuId = '1';
  const double tPrice = 1.2;
  const int tOrder = 1;
  const String tPicture = '1';
  const String tRestaurantId = '1';

  setUp(() async {
    final instance = FakeFirebaseFirestore();
    final emptyInstance = FakeFirebaseFirestore();
    final wrongInstance = FakeFirebaseFirestore();
    tMenuItemsList = [];
    tFoodTagsList = [uid1, uid2];
    tMenuComponents = [uid1, uid2];

    await instance.collection('menu_items').doc(uid1).set({
      'name': tName,
      'description': tDescription,
      'menuId': tMenuId,
      'price': tPrice,
      'order': tOrder,
      'picture': tPicture,
      'restaurantId': tRestaurantId,
      'foodTags': tFoodTagsList,
      'components': tMenuComponents,
    });
    await instance.collection('menu_items').doc(uid2).set({
      'name': tName,
      'description': tDescription,
      'menuId': tMenuId,
      'price': tPrice,
      'order': tOrder,
      'picture': tPicture,
      'restaurantId': tRestaurantId,
      'foodTags': tFoodTagsList,
      'components': tMenuComponents,
    });
    final snapshot = await instance.collection('menu_items').get();

    await wrongInstance.collection('menu_items').doc(uid1).set({});
    await wrongInstance.collection('menu_items').doc(uid2).set({});

    remoteDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: instance);
    remoteEmptyDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: emptyInstance);
    remoteWrongDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: wrongInstance);

    snapshot.docs.forEach((menuItem) {
      tMenuItemsList.add({
        "id": menuItem.id,
        "name": menuItem['name'],
        "description": menuItem['description'],
        "menuId": menuItem['menuId'],
        "price": menuItem['price'],
        "order": menuItem['order'],
        "picture": menuItem['picture'],
        "foodTags": menuItem['foodTags'],
        "components": menuItem['components'],
      });
    });
    tMenuItemsListEntity = MenuItemsListModel(menuItems: [
      MenuItemModel(
        id: uid1,
        name: tName,
        description: tDescription,
        menuId: tMenuId,
        price: tPrice,
        order: tOrder,
        picture: tPicture,
        foodTags: tFoodTagsList,
        components: tMenuComponents,
      ),
      MenuItemModel(
        id: uid2,
        name: tName,
        description: tDescription,
        menuId: tMenuId,
        price: tPrice,
        order: tOrder,
        picture: tPicture,
        foodTags: tFoodTagsList,
        components: tMenuComponents,
      ),
    ]);
  });

  group('getMenuItemIconsList', () {
    test(
      'should return MenuItemsList when there is data stored in FireBase',
      () async {
        //act
        final result = await remoteDataSource.getMenuItemsList(tRestaurantId);
        //assert
        expect(result, tMenuItemsListEntity);
      },
    );
    test(
      'should throws FireStoreException when there is no such collection in FireBase',
      () async {
        //act
        final call = remoteEmptyDataSource.getMenuItemsList;
        //assert
        expect(() async => await call(tRestaurantId),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );

    test(
      'should throws FireStoreException when there is no such data in collection in FireBase',
      () async {
        //act
        final call = remoteWrongDataSource.getMenuItemsList;
        //assert
        expect(() async => await call(tRestaurantId),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );
  });
}
