import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_remote_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';

void main() {
  late MenuRemoteDataSourceImplementation remoteDataSource;
  late MenuRemoteDataSourceImplementation remoteEmptyDataSource;
  late MenuRemoteDataSourceImplementation remoteWrongDataSource;

  late List<Map<String, dynamic>> tMenusList;
  late MenuList tMenusListEntity;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test name';
  const String tDescription = 'Test description';
  const String tParentId = '1';
  const int tOrder = 1;
  const int tDepth = 1;
  const String tRestaurantId = '1';

  setUp(() async {
    final instance = FakeFirebaseFirestore();
    final emptyInstance = FakeFirebaseFirestore();
    final wrongInstance = FakeFirebaseFirestore();
    tMenusList = [];

    await instance.collection('menus').doc(uid1).set({
      'name': tName,
      'description': tDescription,
      'parentId': tParentId,
      'order': tOrder,
      'depth': tDepth,
      'restaurantId': tRestaurantId,
    });
    await instance.collection('menus').doc(uid2).set({
      'name': tName,
      'description': tDescription,
      'parentId': tParentId,
      'order': tOrder,
      'depth': tDepth,
      'restaurantId': tRestaurantId,
    });
    final snapshot = await instance.collection('menus').get();

    await wrongInstance.collection('menus').doc(uid1).set({});
    await wrongInstance.collection('menus').doc(uid2).set({});

    remoteDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: instance);
    remoteEmptyDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: emptyInstance);
    remoteWrongDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: wrongInstance);

    snapshot.docs.forEach((menu) {
      tMenusList.add({
        "id": menu.id,
        "name": menu['name'],
        "description": menu['description'],
        "parentId": menu['parentId'],
        "order": menu['order'],
        "depth": menu['depth'],
      });
    });
    tMenusListEntity = MenuListModel(menus: [
      MenuModel(
        id: uid1,
        name: tName,
        description: tDescription,
        parentId: tParentId,
        order: tOrder,
        depth: tDepth,
      ),
      MenuModel(
        id: uid2,
        name: tName,
        description: tDescription,
        parentId: tParentId,
        order: tOrder,
        depth: tDepth,
      ),
    ]);
  });

  group('getMenusList', () {
    test(
      'should return MenuList when there is data stored in FireBase',
      () async {
        //act
        final result = await remoteDataSource.getMenusList(tRestaurantId);
        //assert
        expect(result, tMenusListEntity);
      },
    );
    test(
      'should throws FireStoreException when there is no such collection in FireBase',
      () async {
        //act
        final call = remoteEmptyDataSource.getMenusList;
        //assert
        expect(() async => await call(tRestaurantId),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );

    test(
      'should throws FireStoreException when there is no such data in collection in FireBase',
      () async {
        //act
        final call = remoteWrongDataSource.getMenusList;
        //assert
        expect(() async => await call(tRestaurantId),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );
  });
}
