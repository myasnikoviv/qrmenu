import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_remote_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_item_icons_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_icon_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';

void main() {
  late MenuRemoteDataSourceImplementation remoteDataSource;
  late MenuRemoteDataSourceImplementation remoteEmptyDataSource;
  late MenuRemoteDataSourceImplementation remoteWrongDataSource;

  late List<Map<String, dynamic>> tMenuItemIconsList;
  late MenuItemIconsList tMenuItemIconsListEntity;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tIcon = '//path';

  setUp(() async {
    final instance = FakeFirebaseFirestore();
    final emptyInstance = FakeFirebaseFirestore();
    final wrongInstance = FakeFirebaseFirestore();
    tMenuItemIconsList = [];

    await instance.collection('menu_icons').doc(uid1).set({'icon': tIcon});
    await instance.collection('menu_icons').doc(uid2).set({'icon': tIcon});
    final snapshot = await instance.collection('menu_icons').get();

    await wrongInstance.collection('menu_icons').doc(uid1).set({});
    await wrongInstance.collection('menu_icons').doc(uid2).set({});

    remoteDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: instance);
    remoteEmptyDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: emptyInstance);
    remoteWrongDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: wrongInstance);

    snapshot.docs.forEach((menuItemIcon) {
      tMenuItemIconsList.add({
        "id": menuItemIcon.id,
        "icon": menuItemIcon['icon'],
      });
    });
    tMenuItemIconsListEntity = MenuItemIconsListModel(menuItemIcons: [
      MenuItemIconModel(id: uid1, icon: tIcon),
      MenuItemIconModel(id: uid2, icon: tIcon),
    ]);
  });

  group('getMenuItemIconsList', () {
    test(
      'should return MenuItemIconsList when there is data stored in FireBase',
      () async {
        //act
        final result = await remoteDataSource.getMenuItemIconsList();
        //assert
        expect(result, tMenuItemIconsListEntity);
      },
    );
    test(
      'should throws FireStoreException when there is no such collection in FireBase',
      () async {
        //act
        final call = remoteEmptyDataSource.getMenuItemIconsList;
        //assert
        expect(() async => await call(),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );

    test(
      'should throws FireStoreException when there is no such data in collection in FireBase',
      () async {
        //act
        final call = remoteWrongDataSource.getMenuItemIconsList;
        //assert
        expect(() async => await call(),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );
  });
}
