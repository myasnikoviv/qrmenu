import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_remote_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/food_tags_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/food_tag_model.dart';

void main() {
  late MenuRemoteDataSourceImplementation remoteDataSource;
  late MenuRemoteDataSourceImplementation remoteEmptyDataSource;
  late MenuRemoteDataSourceImplementation remoteWrongDataSource;
  late List<Map<String, dynamic>> tFoodTagsList;
  late FoodTagsListModel tFoodTagsListEntity;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test Food tag';
  const String tMenuIconId = '1';

  setUp(() async {
    final instance = FakeFirebaseFirestore();
    final emptyInstance = FakeFirebaseFirestore();
    final wrongInstance = FakeFirebaseFirestore();

    await instance
        .collection('foodTags')
        .doc(uid1)
        .set({'name': tName, 'menuIconId': tMenuIconId});
    await instance
        .collection('foodTags')
        .doc(uid2)
        .set({'name': tName, 'menuIconId': tMenuIconId});

    await wrongInstance.collection('foodTags').doc(uid1).set({});
    await wrongInstance.collection('foodTags').doc(uid2).set({});

    final snapshot = await instance.collection('foodTags').get();

    remoteDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: instance);
    remoteEmptyDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: emptyInstance);
    remoteWrongDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: wrongInstance);

    tFoodTagsList = [];
    snapshot.docs.forEach((foodTag) {
      tFoodTagsList.add({
        "id": foodTag.id,
        "name": foodTag['name'],
        "menuIconId": foodTag['menuIconId'],
      });
    });
    tFoodTagsListEntity = FoodTagsListModel(foodTagsList: [
      FoodTagModel(id: uid1, name: tName, menuIconId: tMenuIconId),
      FoodTagModel(id: uid2, name: tName, menuIconId: tMenuIconId),
    ]);
  });

  group('getFoodTagsList', () {
    test(
      'should return FoodTagsList when there is data stored in FireBase',
      () async {
        //act
        final result = await remoteDataSource.getFoodTagsList();
        //assert
        expect(result, tFoodTagsListEntity);
      },
    );
    test(
      'should throws FireStoreException when there is no such collection in FireBase',
      () async {
        //act
        final call = remoteEmptyDataSource.getFoodTagsList;
        //assert
        expect(() async => await call(),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );

    test(
      'should throws FireStoreException when there is no such data in collection in FireBase',
      () async {
        //act
        final call = remoteWrongDataSource.getFoodTagsList;
        //assert
        expect(() async => await call(),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );
  });
}
