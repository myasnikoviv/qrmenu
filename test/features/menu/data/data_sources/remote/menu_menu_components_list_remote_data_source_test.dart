import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/core/error/exceptions.dart';
import 'package:qrmenu/features/menu/data/data_sources/menu_remote_data_source.dart';
import 'package:qrmenu/features/menu/data/models/lists/menu_components_list_model.dart';
import 'package:qrmenu/features/menu/data/models/single/menu_item_component_model.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';

void main() {
  late MenuRemoteDataSourceImplementation remoteDataSource;
  late MenuRemoteDataSourceImplementation remoteEmptyDataSource;
  late MenuRemoteDataSourceImplementation remoteWrongDataSource;
  late List<Map<String, dynamic>> tComponentsList;
  late MenuComponentsList tMenuComponentsListEntity;

  const String uid1 = 'abc';
  const String uid2 = 'def';
  const String tName = 'Test Component';

  setUp(() async {
    final instance = FakeFirebaseFirestore();
    final emptyInstance = FakeFirebaseFirestore();
    final wrongInstance = FakeFirebaseFirestore();
    tComponentsList = [];

    await instance.collection('menu_components').doc(uid1).set({'name': tName});
    await instance.collection('menu_components').doc(uid2).set({'name': tName});
    final snapshot = await instance.collection('menu_components').get();

    await wrongInstance.collection('menu_components').doc(uid1).set({});
    await wrongInstance.collection('menu_components').doc(uid2).set({});

    remoteDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: instance);
    remoteEmptyDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: emptyInstance);
    remoteWrongDataSource =
        MenuRemoteDataSourceImplementation(firebaseFirestore: wrongInstance);

    snapshot.docs.forEach((menuComponent) {
      tComponentsList.add({
        "id": menuComponent.id,
        "name": menuComponent['name'],
      });
    });
    tMenuComponentsListEntity = MenuComponentsListModel(components: [
      MenuItemComponentModel(id: uid1, name: tName),
      MenuItemComponentModel(id: uid2, name: tName),
    ]);
  });

  group('getMenuComponentsList', () {
    test(
      'should return MenuComponentsList when there is data stored in FireBase',
      () async {
        //act
        final result = await remoteDataSource.getMenuComponentsList();
        //assert
        expect(result, tMenuComponentsListEntity);
      },
    );
    test(
      'should throws FireStoreException when there is no such collection in FireBase',
      () async {
        //act
        final call = remoteEmptyDataSource.getMenuComponentsList;
        //assert
        expect(() async => await call(),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );

    test(
      'should throws FireStoreException when there is no such data in collection in FireBase',
      () async {
        //act
        final call = remoteWrongDataSource.getFoodTagsList;
        //assert
        expect(() async => await call(),
            throwsA(TypeMatcher<FireStoreException>()));
      },
    );
  });
}
