import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_component.dart';
import 'package:qrmenu/features/menu/presentation/bloc/error_handler.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_components/menu_components_list_bloc.dart';

import 'mocks.mocks.dart';

void main() {
  late MockGetMenuComponentsList mockGetMenuComponentsList;
  late MenuComponentsListBloc menuComponentsListBloc;

  setUp(() {
    mockGetMenuComponentsList = MockGetMenuComponentsList();
    menuComponentsListBloc = MenuComponentsListBloc(
        getMenuComponentsList: mockGetMenuComponentsList);
  });
  test('initial state should be Empty', () {
    expect(menuComponentsListBloc.state, equals(Empty()));
  });

  group('GetMenuByRestaurantId', () {
    final tMenuItemComponent = MenuItemComponent(
      name: 'Test component',
      id: '1',
    );
    final tMenuComponentsList =
        MenuComponentsList(components: [tMenuItemComponent]);

    void setUpSuccessUseCases() {
      when(mockGetMenuComponentsList(any))
          .thenAnswer((_) async => Right(tMenuComponentsList));
    }

    void setUpFailureUseCases(failureType) {
      when(mockGetMenuComponentsList(any))
          .thenAnswer((_) async => Left(failureType));
    }

    void testError(errorMsg, failureType) {
      test(
        'should emit [Loading,Error] when getting data fails with a propper message',
        () async {
          // arrange
          setUpFailureUseCases(failureType);
          // assert later
          final expected = [
            Loading(),
            Error(errorMsg: errorMsg),
          ];
          expectLater(menuComponentsListBloc.stream, emitsInOrder(expected));
          // act
          menuComponentsListBloc.add(GetMenuComponentsListEvent());
        },
      );
    }

    test(
      'should get data from the concrete usecase',
      () async {
        // arrange
        setUpSuccessUseCases();
        // act
        menuComponentsListBloc.add(GetMenuComponentsListEvent());
        await untilCalled(mockGetMenuComponentsList(any));
        // assert
        verify(mockGetMenuComponentsList(NoParams()));
      },
    );
    test(
      'should emit [Loading,Loaded] when data is gotten successfully',
      () async {
        // arrange
        setUpSuccessUseCases();
        // assert later
        final expected = [
          Loading(),
          MenuComponentsListLoaded(menuComponentsList: tMenuComponentsList),
        ];
        expectLater(menuComponentsListBloc.stream, emitsInOrder(expected));
        // act
        menuComponentsListBloc.add(GetMenuComponentsListEvent());
      },
    );
    group('Failures', () {
      final serverFailureType = ServerFailure();
      testError(SERVER_FAILURE_MESSAGE, serverFailureType);
      final cacheFailureType = CacheFailure();
      testError(CACHE_FAILURE_MESSAGE, cacheFailureType);
    });
  });
}
