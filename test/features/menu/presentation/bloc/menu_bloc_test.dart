import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/util/input_converter.dart';
import 'package:qrmenu/features/menu/domain/entities/hierarchy/menu_hierarchy.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';
import 'package:qrmenu/features/menu/presentation/bloc/error_handler.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu/menu_bloc.dart';

import 'mocks.mocks.dart';

void main() {
  late MenuBloc menuBloc;
  late MockGetHierarchycalMenu mockGetHierarchycalMenu;
  late MockGetMenuItemsList mockGetMenuItemsList;
  late MockGetMenusList mockGetMenusList;
  late MockInputConverter mockInputConverter;

  setUp(() {
    mockGetHierarchycalMenu = MockGetHierarchycalMenu();
    mockGetMenuItemsList = MockGetMenuItemsList();
    mockGetMenusList = MockGetMenusList();
    mockInputConverter = MockInputConverter();

    menuBloc = MenuBloc(
      getHierarchicalMenu: mockGetHierarchycalMenu,
      getMenuItemsList: mockGetMenuItemsList,
      getMenusList: mockGetMenusList,
      inputConverter: mockInputConverter,
    );
  });
  test('initial state should be Empty', () {
    expect(menuBloc.state, equals(Empty()));
  });

  group('GetMenuByRestaurantId', () {
    final tRestaurantId = '1';
    final tRestaurantIdChecked = '1';
    final tMenuLevel0 = Menu(
      id: '1',
      name: 'Test menu level 0',
      description: 'Test menu description',
      parentId: '0',
      order: 1,
      depth: 0,
    );
    final tMenuLevel1 = Menu(
      id: '2',
      name: 'Test menu level 1',
      description: 'Test menu description',
      parentId: '1',
      order: 1,
      depth: 1,
    );
    final tMenuLevel1Order2 = Menu(
      id: '3',
      name: 'Test menu level 1 Order 3',
      description: 'Test menu description',
      parentId: '1',
      order: 2,
      depth: 1,
    );
    final tMenuLevel1Order3 = Menu(
      id: '4',
      name: 'Test menu level 1 Order 3',
      description: 'Test menu description',
      parentId: '1',
      order: 3,
      depth: 1,
    );
    final tMenuItemLevel0 = MenuItem(
      id: '1',
      name: 'Test menu item name level 0',
      description: 'Test menu item description',
      menuId: '1',
      price: 2.1,
      order: 1,
      foodTags: ['1', '2'],
      components: ['1', '2'],
    );
    final tMenuItemLevel1Order1 = MenuItem(
      id: '2',
      name: 'Test menu item name 2 level 1 order 1',
      description: 'Test menu item description',
      menuId: '2',
      price: 2.1,
      order: 1,
      foodTags: ['1', '2'],
      components: ['1', '2'],
    );
    final tMenuItemLevel1Order2 = MenuItem(
      id: '3',
      name: 'Test menu item name 2 level 1 order 2',
      description: 'Test menu item description',
      menuId: '2',
      price: 2.1,
      order: 2,
      foodTags: ['1', '2'],
      components: ['1', '2'],
    );
    final tMenuItemLevel1Order3 = MenuItem(
      id: '4',
      name: 'Test menu item name 2 level 1 order 3',
      description: 'Test menu item description',
      menuId: '2',
      price: 2.1,
      order: 3,
      foodTags: ['1', '2'],
      components: ['1', '2'],
    );

    final tHierarchycalMenuLevel1 = MenuHierarchy(
      nested: null,
      menuItems: [
        tMenuItemLevel1Order1,
        tMenuItemLevel1Order2,
        tMenuItemLevel1Order3,
      ],
      menu: tMenuLevel1,
    );
    final tHierarchycalMenuLevel1Order2 = MenuHierarchy(
      nested: [],
      menuItems: [],
      menu: tMenuLevel1Order2,
    );
    final tHierarchycalMenuLevel1Order3 = MenuHierarchy(
      nested: [],
      menuItems: [],
      menu: tMenuLevel1Order3,
    );
    final tHierarchycalMenuLevel0 = MenuHierarchy(
      nested: [
        tHierarchycalMenuLevel1,
        tHierarchycalMenuLevel1Order2,
        tHierarchycalMenuLevel1Order3,
      ],
      menuItems: [tMenuItemLevel0],
      menu: tMenuLevel0,
    );
    final tHierarchycalMenuLevelRoot = MenuHierarchy(
      nested: [tHierarchycalMenuLevel0],
      menuItems: [],
      menu: Menu(
        id: 'rootMenu',
        name: 'Root menu',
        description: '',
        parentId: 'undefined',
        order: -1,
        depth: -1,
      ),
    );

    final tMenuItem = MenuItem(
      id: '1',
      name: 'Test menu item name',
      description: 'Test menu item description',
      menuId: '1',
      price: 2.1,
      order: 1,
      foodTags: ['1', '2'],
      components: ['1', '2'],
    );
    final tMenuItemsList = MenuItemsList(menuItems: [tMenuItem]);
    final tMenu = Menu(
      id: '1',
      name: 'Root menu',
      description: 'This is a root test menu',
      parentId: '0',
      order: 1,
      depth: 0,
    );
    final tMenusList = MenuList(menus: [tMenu]);

    void setUpSuccessInputConverter() {
      when(mockInputConverter.checkInputStringId(tRestaurantId))
          .thenReturn(Right(tRestaurantIdChecked));
    }

    void setUpUnSuccessInputConverter() {
      when(mockInputConverter.checkInputStringId(any))
          .thenReturn(Left(InvalidInputFailure()));
    }

    void setUpSuccessUseCases() {
      when(mockGetMenuItemsList(any))
          .thenAnswer((_) async => Right(tMenuItemsList));
      when(mockGetMenusList(any)).thenAnswer((_) async => Right(tMenusList));
      when(mockGetHierarchycalMenu(any))
          .thenAnswer((_) async => Right(tHierarchycalMenuLevelRoot));
    }

    void setUpFailureUseCases(failureType) {
      when(mockGetMenuItemsList(any))
          .thenAnswer((_) async => Left(failureType));
      when(mockGetMenusList(any)).thenAnswer((_) async => Left(failureType));
      when(mockGetHierarchycalMenu(any))
          .thenAnswer((_) async => Left(failureType));
    }

    void testError(errorMsg, failureType) {
      test(
        'should emit [Loading,Error] when getting data fails with a propper message',
        () async {
          // arrange
          setUpSuccessInputConverter();
          setUpFailureUseCases(failureType);
          // assert later
          final expected = [
            Loading(),
            Error(errorMsg: errorMsg),
          ];
          expectLater(menuBloc.stream, emitsInOrder(expected));
          // act
          menuBloc.add(GetMenuByRestaurantIdEvent(tRestaurantId));
        },
      );
    }

    test(
      'should call the InputConverter to validate the string',
      () async {
        // arrange
        setUpSuccessInputConverter();
        setUpSuccessUseCases();
        // act
        menuBloc.add(GetMenuByRestaurantIdEvent(tRestaurantIdChecked));
        await untilCalled(mockInputConverter.checkInputStringId(any));
        // assert
        verify(mockInputConverter.checkInputStringId(tRestaurantId));
      },
    );
    test(
      'should emit [Error] state when input is invalid',
      () async {
        // arrange
        setUpUnSuccessInputConverter();
        // assert
        final expected = [
          Error(errorMsg: INPUT_FAILURE_MESSAGE),
        ];
        expectLater(menuBloc.stream, emitsInOrder(expected));
        // act
        menuBloc.add(GetMenuByRestaurantIdEvent(tRestaurantId));
      },
    );
    test(
      'should get data from the concrete usecase',
      () async {
        // arrange
        setUpSuccessInputConverter();
        setUpSuccessUseCases();
        // act
        menuBloc.add(GetMenuByRestaurantIdEvent(tRestaurantId));
        await untilCalled(mockGetHierarchycalMenu(any));
        // assert
        verify(
            mockGetMenuItemsList(Params(restaurantId: tRestaurantIdChecked)));
        verify(mockGetMenusList(Params(restaurantId: tRestaurantIdChecked)));
        verify(mockGetHierarchycalMenu(ParamsHierarchy(
            menusList: tMenusList, menuItemsList: tMenuItemsList)));
      },
    );
    test(
      'should emit [Loading,Loaded] when data is gotten successfully',
      () async {
        // arrange
        setUpSuccessInputConverter();
        setUpSuccessUseCases();
        // assert later
        final expected = [
          Loading(),
          MenuLoaded(menuHierarchy: tHierarchycalMenuLevelRoot),
        ];
        expectLater(menuBloc.stream, emitsInOrder(expected));
        // act
        menuBloc.add(GetMenuByRestaurantIdEvent(tRestaurantId));
      },
    );
    group('Failures', () {
      final serverFailureType = ServerFailure();
      testError(SERVER_FAILURE_MESSAGE, serverFailureType);
      final cacheFailureType = CacheFailure();
      testError(CACHE_FAILURE_MESSAGE, cacheFailureType);
    });
  });
}
