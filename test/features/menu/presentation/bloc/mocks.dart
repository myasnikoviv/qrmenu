import 'package:mockito/annotations.dart';
import 'package:qrmenu/core/util/input_converter.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_hierarchycal_menu.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menus_list.dart';

@GenerateMocks([
  GetFoodTagsList,
  GetHierarchycalMenu,
  GetMenuComponentsList,
  GetMenuItemIconsList,
  GetMenuItemsList,
  GetMenusList,
  InputConverter,
])
void main() {}
