import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_icon.dart';
import 'package:qrmenu/features/menu/presentation/bloc/error_handler.dart';
import 'package:qrmenu/features/menu/presentation/bloc/menu_item_icons/menu_item_icons_list_bloc.dart';

import 'mocks.mocks.dart';

void main() {
  late MockGetMenuItemIconsList mockGetMenuItemIconsList;
  late MenuItemIconsListBloc menuItemIconsListBloc;

  setUp(() {
    mockGetMenuItemIconsList = MockGetMenuItemIconsList();
    menuItemIconsListBloc =
        MenuItemIconsListBloc(getMenuItemIconsList: mockGetMenuItemIconsList);
  });
  test('initial state should be Empty', () {
    expect(menuItemIconsListBloc.state, equals(Empty()));
  });

  group('GetMenuItemIconsListEvent', () {
    final tMenuItemIcon = MenuItemIcon(
      id: '1',
      icon: '',
    );
    final tMenuItemIconsList =
        MenuItemIconsList(menuItemIcons: [tMenuItemIcon]);

    void setUpSuccessUseCases() {
      when(mockGetMenuItemIconsList(any))
          .thenAnswer((_) async => Right(tMenuItemIconsList));
    }

    void setUpFailureUseCases(failureType) {
      when(mockGetMenuItemIconsList(any))
          .thenAnswer((_) async => Left(failureType));
    }

    void testError(errorMsg, failureType) {
      test(
        'should emit [Loading,Error] when getting data fails with a propper message',
        () async {
          // arrange
          setUpFailureUseCases(failureType);
          // assert later
          final expected = [
            Loading(),
            Error(errorMsg: errorMsg),
          ];
          expectLater(menuItemIconsListBloc.stream, emitsInOrder(expected));
          // act
          menuItemIconsListBloc.add(GetMenuItemIconsListEvent());
        },
      );
    }

    test(
      'should get data from the concrete usecase',
      () async {
        // arrange
        setUpSuccessUseCases();
        // act
        menuItemIconsListBloc.add(GetMenuItemIconsListEvent());
        await untilCalled(mockGetMenuItemIconsList(any));
        // assert
        verify(mockGetMenuItemIconsList(NoParams()));
      },
    );
    test(
      'should emit [Loading,Loaded] when data is gotten successfully',
      () async {
        // arrange
        setUpSuccessUseCases();
        // assert later
        final expected = [
          Loading(),
          MenuItemIconsListLoaded(menuItemIconsList: tMenuItemIconsList),
        ];
        expectLater(menuItemIconsListBloc.stream, emitsInOrder(expected));
        // act
        menuItemIconsListBloc.add(GetMenuItemIconsListEvent());
      },
    );
    group('Failures', () {
      final serverFailureType = ServerFailure();
      testError(SERVER_FAILURE_MESSAGE, serverFailureType);
      final cacheFailureType = CacheFailure();
      testError(CACHE_FAILURE_MESSAGE, cacheFailureType);
    });
  });
}
