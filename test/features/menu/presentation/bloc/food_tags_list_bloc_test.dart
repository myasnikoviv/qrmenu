import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/food_tag.dart';
import 'package:qrmenu/features/menu/presentation/bloc/error_handler.dart';
import 'package:qrmenu/features/menu/presentation/bloc/food_tags/food_tags_list_bloc.dart';

import 'mocks.mocks.dart';

void main() {
  late MockGetFoodTagsList mockGetFoodTagsList;
  late FoodTagsListBloc foodTagsListBloc;

  setUp(() {
    mockGetFoodTagsList = MockGetFoodTagsList();
    foodTagsListBloc = FoodTagsListBloc(getFoodTagsList: mockGetFoodTagsList);
  });
  test('initial state should be Empty', () {
    expect(foodTagsListBloc.state, equals(Empty()));
  });

  group('GetFoodTagsListEvent', () {
    final tFoodTag = FoodTag(
      name: 'Test tag',
      id: '1',
      menuIconId: '1',
    );
    final tFoodTagsList = FoodTagsList(foodTagsList: [tFoodTag]);

    void setUpSuccessUseCases() {
      when(mockGetFoodTagsList(any))
          .thenAnswer((_) async => Right(tFoodTagsList));
    }

    void setUpFailureUseCases(failureType) {
      when(mockGetFoodTagsList(any)).thenAnswer((_) async => Left(failureType));
    }

    void testError(errorMsg, failureType) {
      test(
        'should emit [Loading,Error] when getting data fails with a propper message',
        () async {
          // arrange
          setUpFailureUseCases(failureType);
          // assert later
          final expected = [
            Loading(),
            Error(errorMsg: errorMsg),
          ];
          expectLater(foodTagsListBloc.stream, emitsInOrder(expected));
          // act
          foodTagsListBloc.add(GetFoodTagsListEvent());
        },
      );
    }

    test(
      'should get data from the concrete usecase',
      () async {
        // arrange
        setUpSuccessUseCases();
        // act
        foodTagsListBloc.add(GetFoodTagsListEvent());
        await untilCalled(mockGetFoodTagsList(any));
        // assert
        verify(mockGetFoodTagsList(NoParams()));
      },
    );
    test(
      'should emit [Loading,Loaded] when data is gotten successfully',
      () async {
        // arrange
        setUpSuccessUseCases();
        // assert later
        final expected = [
          Loading(),
          FoodTagsListLoaded(foodTagsList: tFoodTagsList),
        ];
        expectLater(foodTagsListBloc.stream, emitsInOrder(expected));
        // act
        foodTagsListBloc.add(GetFoodTagsListEvent());
      },
    );
    group('Failures', () {
      final serverFailureType = ServerFailure();
      testError(SERVER_FAILURE_MESSAGE, serverFailureType);
      final cacheFailureType = CacheFailure();
      testError(CACHE_FAILURE_MESSAGE, cacheFailureType);
    });
  });
}
