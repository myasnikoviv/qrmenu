import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_item_icons_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_icon.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_item_icons_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late GetMenuItemIconsList usecase;
  late MockMenuRepository mockMenuRepository;

  setUp(() {
    mockMenuRepository = MockMenuRepository();
    usecase = GetMenuItemIconsList(mockMenuRepository);
  });

  final tMenuItemIcon = MenuItemIcon(
    id: '1',
    icon: '',
  );
  final tMenuItemIconsList = MenuItemIconsList(menuItemIcons: [tMenuItemIcon]);

  test('Should get Menu Items List from repository', () async {
    when(mockMenuRepository.getMenuItemIconsList())
        .thenAnswer((_) async => Right(tMenuItemIconsList));

    final result = await usecase(NoParams());
    expect(result, Right(tMenuItemIconsList));
    verify(mockMenuRepository.getMenuItemIconsList());
    verifyNoMoreInteractions(mockMenuRepository);
  });
  test('Should get Failure when the result is empty', () async {
    when(mockMenuRepository.getMenuItemIconsList())
        .thenAnswer((_) async => Left(EmptyFailure()));
    final result = await usecase(NoParams());
    expect(result, Left(EmptyFailure()));
    verify(mockMenuRepository.getMenuItemIconsList());
    verifyNoMoreInteractions(mockMenuRepository);
  });
}
