import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late GetMenuItemsList usecase;
  late MockMenuRepository mockMenuRepository;

  setUp(() {
    mockMenuRepository = MockMenuRepository();
    usecase = GetMenuItemsList(mockMenuRepository);
  });

  final tMenuItem = MenuItem(
    id: '1',
    name: 'Test menu item name',
    description: 'Test menu item description',
    menuId: '1',
    price: 2.1,
    order: 1,
    foodTags: ['1', '2'],
    components: ['1', '2'],
  );
  final tMenuItemsList = MenuItemsList(menuItems: [tMenuItem]);
  final tRestaurantId = '1';

  test('Should get Menu Items List for the restraunt Id from repository',
      () async {
    when(mockMenuRepository.getMenuItemsList(any))
        .thenAnswer((_) async => Right(tMenuItemsList));

    final result = await usecase(Params(restaurantId: tRestaurantId));
    expect(result, Right(tMenuItemsList));
    verify(mockMenuRepository.getMenuItemsList(tRestaurantId));
    verifyNoMoreInteractions(mockMenuRepository);
  });
  test('Should get Failure when there is no such restraunt Id', () async {
    when(mockMenuRepository.getMenuItemsList(any))
        .thenAnswer((_) async => Left(EmptyFailure()));
    final result = await usecase(Params(restaurantId: tRestaurantId));
    expect(result, Left(EmptyFailure()));
    verify(mockMenuRepository.getMenuItemsList(tRestaurantId));
    verifyNoMoreInteractions(mockMenuRepository);
  });
}
