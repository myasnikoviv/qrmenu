import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menus_list.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late GetMenusList usecase;
  late MockMenuRepository mockMenuRepository;

  setUp(() {
    mockMenuRepository = MockMenuRepository();
    usecase = GetMenusList(mockMenuRepository);
  });

  final tMenu = Menu(
    id: '1',
    name: 'Root menu',
    description: 'This is a root test menu',
    parentId: '0',
    order: 1,
    depth: 0,
  );
  final tMenusList = MenuList(menus: [tMenu]);
  final tRestaurantId = '1';

  test('Should get Menus List for the restraunt Id from repository', () async {
    when(mockMenuRepository.getMenusList(any))
        .thenAnswer((_) async => Right(tMenusList));

    final result = await usecase(Params(restaurantId: tRestaurantId));
    expect(result, Right(tMenusList));
    verify(mockMenuRepository.getMenusList(tRestaurantId));
    verifyNoMoreInteractions(mockMenuRepository);
  });
  test('Should get Failure when there is no such restraunt Id', () async {
    when(mockMenuRepository.getMenusList(any))
        .thenAnswer((_) async => Left(EmptyFailure()));
    final result = await usecase(Params(restaurantId: tRestaurantId));
    expect(result, Left(EmptyFailure()));
    verify(mockMenuRepository.getMenusList(tRestaurantId));
    verifyNoMoreInteractions(mockMenuRepository);
  });
}
