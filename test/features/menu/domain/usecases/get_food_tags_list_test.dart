import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/food_tags_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/food_tag.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_food_tags_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late GetFoodTagsList usecase;
  late MockMenuRepository mockMenuRepository;

  setUp(() {
    mockMenuRepository = MockMenuRepository();
    usecase = GetFoodTagsList(mockMenuRepository);
  });

  final tFoodTag = FoodTag(
    name: 'Test tag',
    id: '1',
    menuIconId: '1',
  );
  final tFoodTagsList = FoodTagsList(foodTagsList: [tFoodTag]);

  test('Should get Food Tags List from repository', () async {
    when(mockMenuRepository.getFoodTagsList())
        .thenAnswer((_) async => Right(tFoodTagsList));

    final result = await usecase(NoParams());
    expect(result, Right(tFoodTagsList));
    verify(mockMenuRepository.getFoodTagsList());
    verifyNoMoreInteractions(mockMenuRepository);
  });
  test('Should get Failure when the result is empty', () async {
    when(mockMenuRepository.getFoodTagsList())
        .thenAnswer((_) async => Left(EmptyFailure()));
    final result = await usecase(NoParams());
    expect(result, Left(EmptyFailure()));
    verify(mockMenuRepository.getFoodTagsList());
    verifyNoMoreInteractions(mockMenuRepository);
  });
}
