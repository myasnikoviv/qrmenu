import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/error/failure.dart';
import 'package:qrmenu/core/usecases/usecase.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_components_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item_component.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_menu_components_list.dart';

import 'generate_mocks.mocks.dart';

void main() {
  late GetMenuComponentsList usecase;
  late MockMenuRepository mockMenuRepository;

  setUp(() {
    mockMenuRepository = MockMenuRepository();
    usecase = GetMenuComponentsList(mockMenuRepository);
  });

  final tMenuItemComponent = MenuItemComponent(
    name: 'Test component',
    id: '1',
  );
  final tMenuComponentsList =
      MenuComponentsList(components: [tMenuItemComponent]);

  test('Should get Menu Components List from repository', () async {
    when(mockMenuRepository.getMenuComponentsList())
        .thenAnswer((_) async => Right(tMenuComponentsList));

    final result = await usecase(NoParams());
    expect(result, Right(tMenuComponentsList));
    verify(mockMenuRepository.getMenuComponentsList());
    verifyNoMoreInteractions(mockMenuRepository);
  });
  test('Should get Failure when the result is empty', () async {
    when(mockMenuRepository.getMenuComponentsList())
        .thenAnswer((_) async => Left(EmptyFailure()));
    final result = await usecase(NoParams());
    expect(result, Left(EmptyFailure()));
    verify(mockMenuRepository.getMenuComponentsList());
    verifyNoMoreInteractions(mockMenuRepository);
  });
}
