import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/features/menu/domain/entities/hierarchy/menu_hierarchy.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_items_list.dart';
import 'package:qrmenu/features/menu/domain/entities/lists/menu_list.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu.dart';
import 'package:qrmenu/features/menu/domain/entities/single/menu_item.dart';
import 'package:qrmenu/features/menu/domain/usecases/get_hierarchycal_menu.dart';
import 'package:qrmenu/features/menu/domain/usecases/params.dart';

void main() {
  late GetHierarchycalMenu usecase;

  final tMenuLevel0 = Menu(
    id: '1',
    name: 'Test menu level 0',
    description: 'Test menu description',
    parentId: '0',
    order: 1,
    depth: 0,
  );
  final tMenuLevel1 = Menu(
    id: '2',
    name: 'Test menu level 1',
    description: 'Test menu description',
    parentId: '1',
    order: 1,
    depth: 1,
  );
  final tMenuLevel1Order2 = Menu(
    id: '3',
    name: 'Test menu level 1 Order 3',
    description: 'Test menu description',
    parentId: '1',
    order: 2,
    depth: 1,
  );
  final tMenuLevel1Order3 = Menu(
    id: '4',
    name: 'Test menu level 1 Order 3',
    description: 'Test menu description',
    parentId: '1',
    order: 3,
    depth: 1,
  );
  final tMenuItemLevel0 = MenuItem(
    id: '1',
    name: 'Test menu item name level 0',
    description: 'Test menu item description',
    menuId: '1',
    price: 2.1,
    order: 1,
    foodTags: ['1', '2'],
    components: ['1', '2'],
  );
  final tMenuItemLevel1Order1 = MenuItem(
    id: '2',
    name: 'Test menu item name 2 level 1 order 1',
    description: 'Test menu item description',
    menuId: '2',
    price: 2.1,
    order: 1,
    foodTags: ['1', '2'],
    components: ['1', '2'],
  );
  final tMenuItemLevel1Order2 = MenuItem(
    id: '3',
    name: 'Test menu item name 2 level 1 order 2',
    description: 'Test menu item description',
    menuId: '2',
    price: 2.1,
    order: 2,
    foodTags: ['1', '2'],
    components: ['1', '2'],
  );
  final tMenuItemLevel1Order3 = MenuItem(
    id: '4',
    name: 'Test menu item name 2 level 1 order 3',
    description: 'Test menu item description',
    menuId: '2',
    price: 2.1,
    order: 3,
    foodTags: ['1', '2'],
    components: ['1', '2'],
  );

  final tHierarchycalMenuLevel1 = MenuHierarchy(
    nested: null,
    menuItems: [
      tMenuItemLevel1Order1,
      tMenuItemLevel1Order2,
      tMenuItemLevel1Order3,
    ],
    menu: tMenuLevel1,
  );
  final tHierarchycalMenuLevel1Order2 = MenuHierarchy(
    nested: [],
    menuItems: [],
    menu: tMenuLevel1Order2,
  );
  final tHierarchycalMenuLevel1Order3 = MenuHierarchy(
    nested: [],
    menuItems: [],
    menu: tMenuLevel1Order3,
  );
  final tHierarchycalMenuLevel0 = MenuHierarchy(
    nested: [
      tHierarchycalMenuLevel1,
      tHierarchycalMenuLevel1Order2,
      tHierarchycalMenuLevel1Order3,
    ],
    menuItems: [tMenuItemLevel0],
    menu: tMenuLevel0,
  );
  final tHierarchycalMenuLevelRoot = MenuHierarchy(
    nested: [tHierarchycalMenuLevel0],
    menuItems: [],
    menu: Menu(
      id: 'rootMenu',
      name: 'Root menu',
      description: '',
      parentId: 'undefined',
      order: -1,
      depth: -1,
    ),
  );

  setUp(() {
    usecase = GetHierarchycalMenu();
  });

  test('Should return hierarchycal menu, ordered ASC', () async {
    final result = await usecase(ParamsHierarchy(
      menuItemsList: MenuItemsList(menuItems: [
        tMenuItemLevel1Order3,
        tMenuItemLevel1Order2,
        tMenuItemLevel1Order1,
        tMenuItemLevel0,
      ]),
      menusList: MenuList(menus: [
        tMenuLevel1,
        tMenuLevel1Order3,
        tMenuLevel1Order2,
        tMenuLevel0,
      ]),
    ));
    expect(result, Right(tHierarchycalMenuLevelRoot));
  });
}
