import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:qrmenu/core/network/network_info.dart';

import 'network_info_test.mocks.dart';

@GenerateMocks([Connectivity])
void main() {
  late NetworkInfoImplementation networkInfoImplementation;
  late MockConnectivity mockConnectivity;

  setUp(() {
    mockConnectivity = MockConnectivity();
    networkInfoImplementation = NetworkInfoImplementation(mockConnectivity);
  });

  group('isConnected', () {
    test(
      'should forward the call to Connectivity.checkConnectivity() and answer true if mobile',
      () async {
        // arrange
        when(mockConnectivity.checkConnectivity())
            .thenAnswer((_) async => ConnectivityResult.mobile);
        // act
        final result = await networkInfoImplementation.isConnected;
        // assert
        verify(mockConnectivity.checkConnectivity());
        expect(result, true);
      },
    );
    test(
      'should forward the call to Connectivity.checkConnectivity() and answer true if wifi',
      () async {
        // arrange
        when(mockConnectivity.checkConnectivity())
            .thenAnswer((_) async => ConnectivityResult.wifi);
        // act
        final result = await networkInfoImplementation.isConnected;
        // assert
        verify(mockConnectivity.checkConnectivity());
        expect(result, true);
      },
    );
    test(
      'should forward the call to Connectivity.checkConnectivity() and answer false if none',
      () async {
        // arrange
        when(mockConnectivity.checkConnectivity())
            .thenAnswer((_) async => ConnectivityResult.none);
        // act
        final result = await networkInfoImplementation.isConnected;
        // assert
        verify(mockConnectivity.checkConnectivity());
        expect(result, false);
      },
    );
  });
}
