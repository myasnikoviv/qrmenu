import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:qrmenu/core/util/input_converter.dart';

void main() {
  late InputConverter inputConverter;

  setUp(() {
    inputConverter = InputConverter();
  });

  group('checkInputStringId', () {
    test(
      'should return a string when input is valid',
      () async {
        // arrange
        final str = 'asdasda234234243';
        // act
        final result = inputConverter.checkInputStringId(str);
        // assert
        expect(result, Right(str));
      },
    );
    test(
      'should return a InvalidInputFailure when input is not valid',
      () async {
        // arrange
        final str = 'asdasd_a234234243';
        // act
        final result = inputConverter.checkInputStringId(str);
        // assert
        expect(result, Left(InvalidInputFailure()));
      },
    );
  });
}
