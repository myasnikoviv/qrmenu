# QR menu

QR menu Flutter project for restaurants.

## About

This is a Dart&Flutter project for restaurant guests allows'em to scan QR code and read a menu in the App. It uses clean
architecture and has 2 features:

- Scan QR code
- Get and display hierarchical menu from remote storage or cached data

The application was written in TDD style, so it contains relative tests. Used libs:

- get_it for dependencies injection
- Bloc as a state management tool
- Firebase firestore as a remote database
- Hive as a caching service

Developer: Myasnikov Ilya Currently based in Ukraine

- [LinkedIn profile](https://www.linkedin.com/in/ilya-myasnikov-18505755/)
- [myasnikov.iv@gmail.com](mailto:myasnikov.iv@gmail.com)

## Roadmap

- Design
- Restaurants feature
- Authorization